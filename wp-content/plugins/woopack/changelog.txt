== 1.3.11 ==
Release date: December 14, 2020
* Enhancement: Product Category - Added default option in order-by field
* Fix: Product Grid - Variations were not working after grid items getting AJAX filtered
* Fix: Product Grid - Grid layout issue when using in PowerPack Advanced Tab module
* Fix: PHP error - FLPageData not found when using WooPack with BB Lite and Beaver Themer

== 1.3.10 ==
Release date: November 4, 2020
* Enhancement: Added WordPress Appointment Booking plugin support
* Enhancement: Product Grid & Carousel - Added RTL support
* Fix: Product Grid & Carousel - Beaver Builder stopped working due to conflict between Woocommerce Side Cart Premium plugin and WooPack module
* Fix: Product Grid & Carousel - ACF Relationship query source was not working
* Fix: Product Grid - Pagination issue on search result archive page
* Fix: Product Grid - Inner slider overflow issue
* Fix: Cart - Some styles were getting overridden by theme

== 1.3.9.6 ==
Release date: Ocotober 5, 2020
* Fix: Product Grid & Carousel - Add to Cart button was not working in case of shared attributes
* Fix: Product Carousel - Quick View template dropdown was not getting updated on saving module settings
* Fix: Core - Updated WooCommerce version

== 1.3.9.5 ==
Release date: September 30, 2020
* Enhancement: My Account - Converted top positioned tabs into dropdown menu for small devices
* Fix: Product Grid - All images of a product were still loading after disabling the image slider option
* Fix: Product Grid - Equal Height was not working row-wise
* Fix: Product Grid - Dynamic filters were not working in combination with load more or scroll pagination
* Fix: Product Grid & Carousel - Compatibility issue with GeneratePress theme
* Fix: Product Carousel - PHP Notice due to missing option for no products found
* Fix: Product Carousel - Custom Quick View template was not appearing in settings dropdown

== 1.3.9.4 ==
Release date: August 27, 2020
* Fix: Product Grid - Pagination hover color was not working
* Fix: Product Grid - AJAX pagination issue in WP 5.5

== 1.3.9.3 ==
Release date: August 14, 2020
* Fix: Product Grid - "All" dynamic filter was not working in WP 5.5

== 1.3.9.2 ==
Release date: August 12, 2020
* Enhancement: Product Grid - Added option to position the filters top, left, or right
* Fix: Product Grid - Duplicate posts issue on AJAX
* Fix: Product Grid - Scroll pagination was stopped working
* Fix: Product Grid - AJAX broke in WP 5.5
* Fix: Product Grid & Carousel - PHP error
* Development: Product Grid - Added filter hook woopack_product_grid_breakpoints to use custom breakpoints for medium and responsive devices

== 1.3.9.1 ==
Release date: June 25, 2020
* Fix: Product Carousel - Multiple images were showing after last update

== 1.3.9 ==
Release date: June 24, 2020
* Enhancement: Quick View - Added Beaver Themer layout support for Quick View template
* Enhancement: Product Grid - Added images slider
* Enhancement: Product Grid - Added accessibility support for filters
* Enhancement: Product Category - Added option to display parent or children categories exclusively
* Fix: Product Grid - Layout break when using FacetWP filters
* Fix: Product Grid - Responsive spacing was not working
* Fix: Product Carousel - Mouse drag was not working
* Fix: Quick View - Added support for Variation Gallery and Variation Swatches plugin
* Fix: Mini Cart - Cart counter was not getting updated when adding variable product into cart

== 1.3.8.1 ==
Release date: May 11, 2020
* Fix: Quick View overlay background color was not getting applied after previous update
* Fix: Mini Cart - Container height issue when there are large number of products in cart
* Fix: My Account - Responsive issues
* Fix: Product Category - Added additional CSS to maintain columns on responsive devices

== 1.3.8 ==
Release date: May 6, 2020
* New: Added My Account module
* New: Added Custom Layout feature in Product Grid & Carousel (with additional field connections)
* Enhancement: Added support for Name Your Price addon
* Enhancement: Product Category - Added category ordering options
* Enhancement: Product Carousel - Autoplay will be stopped when clicking on Quick View button
* Enhancement: Single Product - Added option to show rating count
* Fix: Single Product - Conflict with WooCommerce Product Addons
* Fix: Product Grid - Filters alignment issue when there are large number of terms
* Fix: Product Grid & Carousel - Incorrect reviews count

== 1.3.7.3 ==
Release date: April 1, 2020
* Enhancement: Product Category - Added option to hide category description
* Enhancement: Product Grid - Added default values in column fields for medium and responsive
* Fix: Single Product - Star rating alignment was not working in Firefox

== 1.3.7.2 ==
Release date: March 5, 2020
* Enhancement: Product Grid & Carousel - Added option to show Related Products
* Enhancement: Product Grid & Carousel - Added title hover color option
* Enhancement: Product Grid & Carousel - Added logic to override templates via theme
* Fix: Module fields controls were stopped showing

== 1.3.7.1 ==
Release date: February 11, 2020
* Fix: PHP notices due to newly added Rating Count option

== 1.3.7 ==
Release date: February 10, 2020
* New: Added WP-CLI integration for to activate or deactivate license and clear white label branding
* Enhancement: Product Grid - Added "Selection Order" option under "Order By" field
* Enhancement: Product Grid & Carousel - Added "Exclude Current Product" option
* Enhancement: White Label - Added option to list WooPack modules with Standard Modules group
* Enhancement: Added WooCommerce Bookings extension support
* Enhancement: Product Category - Added link target option
* Enhancement: Product Grid & Carousel - Added option to display rating count + text
* Fix: Single Product - Image was not being updated for variations
* Fix: Mini Cart - Normal counter background and text color was not working
* Fix: JS error due to wc_add_to_cart_params is undefined when AJAX is set to disable
* Fix: Product Grid - AJAX filters were not working on some servers
* Fix: Product Grid - Added condition to prevent default Woo notice being displayed under module content on Single Product page
* Fix: Product Grid - Pagination hover color was not working
* Development: Added hook woopack_grid_ajax_query_args to filter query arguments for Product Grid when dynamic (AJAX) post filters used
* Development: Added action woopack_grid_ajax_before_query to hook any custom logic before executing WP_Query when dynamic (AJAX) post filters used 
* Development: Added action woopack_grid_ajax_after_query to hook any custom logic after executing WP_Query when dynamic (AJAX) post filters used

== 1.3.6 ==
Release date: April 25, 2019
* New: Added Mini Cart module
* New: Added Off-Canvas Cart module
* Enhancement: Add to Cart - Added support for grouped product
* Fix: Product Grid & Carousel - Added missing post classes to product posts
* Fix: Product Grid - alignment option was not working properly
* Fix: Cart - Responsive border issue
* Fix: PHP warning in quick view popup box CSS
* Development: Added hooks woopack_loop_before_product_title, woopack_loop_after_product_title, woopack_loop_before_product_description, woopack_loop_after_product_description

== 1.3.5 ==
Release date: March 14, 2019
* Enhancement: Updated fields to match BB 2.2 UI
* Enhancement: Product Grid - Added filter by hash parameter in URL
* Enhancement: Product Grid & Carousel - Used placeholder image from WooCommerce settings if available
* Enhancement: Cart - Added Width field for quantity input
* Fix: Product Grid - Layout break when used inside Advanced Tabs module of PowerPack
* Fix: Product Grid - Layout break on enabling short product description
* Fix: Minor CSS issues

== 1.3.4.3 ==
Release date: October 25, 2018
* Enhancement: Product Category - Added "Default" as basic style option
* Enhancement: Single Product - Added field connection to product dropdown
* Enhancement: Add to Cart - Added field connection to product dropdown
* Fix: Cart - Button styling issue
* Fix: Multiple product variations related issues

== 1.3.4.2 ==
Release date: October 10, 2018
* Fix: Removed WooCommerce plugin dependency from WooPack admin settings

== 1.3.4.1 ==
Release date: September 28, 2018
* Fix: External product link was opening in the same window
* Fix: JS error on single product page when adding variation product to the cart
* Fix: imagesLoaded JS error due to recent update of Beaver Builder (v2.1.5.1)

== 1.3.4 ==
Release date: August 3, 2018
* Enhancement: Added logic to override modules from theme
* Enhancement: Converted multiple unit fields (padding, margin) into single dimension field
* Enhancement: Product Grid and Carousel - Added Out of Stock text and styling options
* Enhancement: Product Carousel - Added more styling options for navigation arrows
* Fix: Product Carousel - JS error due to conflict with PowerPack
* Fix: Product Grid - Masonry columns were not rendering as per column setting in module
* Fix: Product Grid & Carousel - AJAX Add to Cart was not working for grouped product
* Fix: Quick View - Add to Cart for product variations was redirecting to the home page

== 1.3.3 ==
Release date: July 9, 2018
* Fix: Single Product - Price was not being updated when choosing an option from the product variation dropdown
* Fix: Single Product - BB's inline editor was changing Add to Cart button text
* Fix: Single Product - Variations alignment issue when content alignment is set to default
* Fix: Product Grid & Carousel - Product image was not being updated when choosing an option from the product variation dropdown
* Fix: Product Grid & Carousel - View Cart button goes outside the container when width is set to 100%
* Fix: Product Grid & Carousel - View Cart button incorrect styling
* Fix: Product Category - Minor CSS issues

== 1.3.2 ==
Release date: May 8, 2018
* Enhancement: Quick View now supports AJAX add-to-cart
* Fix: Single Product - PHP error

== 1.3.1 ==
Release date: April 11, 2018
* Fix: Product Category - Image opacity was still working when the background color is not set
* Fix: Product Category - Responsive layout issues
* Fix: Product Grid - PHP notices due to undefined variables
* Fix: Product Carousel - PHP notices due to undefined variables

== 1.3.0 ==
Release date: March 28, 2018
* New: Product Category module
* Enhancement: Added CSS class woopack-product-featured-image to product image element
* Enhancement: Added WooCommerce meta keys in help text of orderby Meta Key field
* Enhancement: Added various hooks in the plugin
* Enhancement: Added WooCommerce version support in the plugin header to determine compatibility with latest version
* Enhancement: Added an option to show or hide product SKU from meta in Single Product module
* Enhancement: Added an option to enable product variations fields in Product Grid, Carousel, and Add To Cart button modules
* Enhancement: Added an option to enable quantity input in Add To Cart button module
* Enhancement: Updated loop "Add to Cart" button parameters
* Fix: PHP notice due to a missing parameter in filter hooks woocommerce_sale_flash
* Fix: "All" filter in Products Grid was not working in respect of conditional terms
* Fix: Width issue in Single Product module due to minor conflict with Beaver Themer
* Fix: Some styling in Products Grid module were being overridden by theme
* Fix: Add to Cart button styling issue in Single Product module
* Fix: Product variations styling issue in Single Product module
* Fix: Minor CSS issues in Single Product module
* Fix: Products Grid responsive issue
* Fix: Empty CSS output
* Fix: WP Rocket lazy load issue

== 1.2.1.1 ==
Release date: January 10, 2018
* Fix: PHP fatal error due to undefined object

== 1.2.1 ==
Release date: January 10, 2018
* Enhancement: Added product source option (to dsiplay featured, sale, top rated, etc. products) in Product Grid and Carousel module
* Enhancement: Content, badge, button, image, meta, and quick view will now be configurable in Main Query source
* Enhancement: Moved custom filter options into separate Filter tab and added filter typography options in Product Grid module
* Enhancement: Added option in Product Grid and Carousel to display quantity input field
* Fix: WooCommerce default sorting dropdown was not working for product archive pages built via Beaver Themer
* Fix: Products were showing against the Catalog visibility option
* Fix: Product Grid filters were not working in IE
* Fix: Quantity input was not appearing in Single Product module
* Fix: Content width issue in Single Product module

== 1.2.0.1 ==
Release date: November 16, 2017
* Fix: Modules were not loading in builder's settings

== 1.2.0 ==
Release date: November 15, 2017
* Enhancement: Added a field to change "All" filter label in Product Grid module
* Enhancement: Added logic to cache Dynamic filter data to avoid multiple AJAX requests in Product Grid module
* Fix: Product image was not linked with product page
* Fix: Filter border position Default was not working in Products Grid
* Fix: Equal Height was not working with Dynamic filter
* Fix: Renamed Product Taxonomy section to Product Meta
* Fix: Minor CSS issue in Products Grid module caused by GP Premium plugin

== 1.1.0 ==
Release date: November 10, 2017
* New: Added filters in Product Grid module
* Enhancement: Added BB 2.0 compatibility
* Fix: Quick View was not working sometimes

== 1.0.0 ==
Release date: October 24, 2017
* Initial version
