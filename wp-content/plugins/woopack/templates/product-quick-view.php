<?php
/**
 * Product Quick View
 *
 * This template can be overridden by copying it to yourtheme/woopack/templates/product-quick-view.php.
 *
 * HOWEVER, on occasion WooPack will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @package WooPack/Templates
 * @version 1.3.0
 */
?>

<?php $product_id = get_the_ID(); ?>

<?php do_action( 'woopack_before_quick_view_wrap', $product_id ); ?>

<div class="fl-node-<?php echo $node_id; ?> woopack-product woocommerce<?php echo $template_id ? ' woopack-qv-template' : ''; ?>">
	<?php do_action( 'woopack_before_quick_view_content', $product_id ); ?>

	<?php WooPack_Helper::render_template( $product_id, $template_id ); ?>

	<?php do_action( 'woopack_after_quick_view_content', $product_id ); ?>
</div>

<?php do_action( 'woopacka_after_quick_view_wrap', $product_id ); ?>
