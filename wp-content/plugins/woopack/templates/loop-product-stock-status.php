<?php
/**
 * Loop Product Image.
 *
 * This template can be overridden by copying it to yourtheme/woopack/templates/loop-product-stock-status.php.
 *
 * HOWEVER, on occasion WooPack will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @package WooPack/Templates
 * @version 1.3.0
 */
?>

<?php if ( ! $product->is_in_stock() ) { ?>
	<div class="woopack-out-of-stock">
		<span>
			<?php
			if ( isset( $settings->out_of_stock_text ) ) {
				echo $settings->out_of_stock_text;
			} else {
				echo esc_html__( 'Out of Stock', 'woopack' );
			} ?>
		</span>
	</div>
<?php }	?>
