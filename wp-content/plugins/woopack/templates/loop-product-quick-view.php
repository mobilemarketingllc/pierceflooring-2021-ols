<?php
/**
 * Loop Product Image.
 *
 * This template can be overridden by copying it to yourtheme/woopack/templates/loop-product-quick-view.php.
 *
 * HOWEVER, on occasion WooPack will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @package WooPack/Templates
 * @version 1.3.0
 */
?>

<?php if ( isset( $settings->show_quick_view ) && 'yes' == $settings->show_quick_view ) : ?>
	<div 
		class="woopack-product-quick-view"
		<?php echo ! empty( $settings->quick_view_custom_text ) ? ' title="' . $settings->quick_view_custom_text . '"' : ''; ?>
		<?php echo isset( $settings->quick_view_template ) && ! empty( $settings->quick_view_template ) ? ' data-template="' . $settings->quick_view_template . '"' : ''; ?>>
		<?php if ( isset( $settings->quick_view_custom_icon ) && ! empty( $settings->quick_view_custom_icon ) ) { ?>
			<span class="woopack-quick-view-icon <?php echo $settings->quick_view_custom_icon; ?>"></span>
		<?php } ?>
		<?php if ( ! empty( $settings->quick_view_custom_text ) ) { ?>
			<span class="woopack-quick-view-text"><?php echo $settings->quick_view_custom_text; ?></span>
		<?php } ?>
	</div>
<?php endif; ?>
