<?php
/**
 * WooPack admin settings license tab.
 *
 * @since 1.0.0
 * @package woopack
 */

?>

<?php if ( is_network_admin() || ! is_multisite() ) { ?>

    <?php settings_fields( 'woopack_license' ); ?>

    <p><?php echo sprintf(__('Enter your <a href="%s" target="_blank">license key</a> to enable remote updates and support.', 'woopack'), 'https://wpbeaveraddons.com/my-account/'); ?>
    <table class="form-table">
        <tbody>
            <tr valign="top">
                <th scope="row" valign="top">
                    <?php esc_html_e('License Key', 'woopack'); ?>
                </th>
                <td>
                    <input id="woopack_license_key" name="woopack_license_key" type="password" class="regular-text" value="<?php esc_attr_e( $license ); ?>" />
                </td>
            </tr>
            <?php if( false !== $license && ! empty($license) ) { ?>
                <tr valign="top">
                    <th scope="row" valign="top">
                        <?php esc_html_e( 'License Status', 'woopack' ); ?>
                    </th>
                    <td>
                        <?php if ( $status == 'valid' ) { ?>
                            <span style="color: #267329; background: #caf1cb; padding: 5px 10px; text-shadow: none; border-radius: 3px; display: inline-block; text-transform: uppercase;"><?php esc_html_e('active'); ?></span>
                            <?php wp_nonce_field( 'woopack_nonce', 'woopack_nonce' ); ?>
                                <input type="submit" class="button-secondary" name="woopack_license_deactivate" value="<?php esc_html_e('Deactivate License', 'woopack'); ?>" />
                        <?php } else { ?>
                            <?php if ( $status == '' ) { $status = 'inactive'; } ?>
                            <span style="<?php echo $status == 'inactive' ? 'color: #fff; background: #b1b1b1;' : 'color: red; background: #ffcdcd;'; ?> padding: 5px 10px; text-shadow: none; border-radius: 3px; display: inline-block; text-transform: uppercase;"><?php echo $status; ?></span>
                            <?php
                            wp_nonce_field( 'woopack_nonce', 'woopack_nonce' ); ?>
                            <input type="submit" class="button-secondary" name="woopack_license_activate" value="<?php esc_html_e( 'Activate License', 'woopack' ); ?>"/>
                            <p class="description"><?php esc_html_e( 'Please click the “Activate License” button to activate your license.', 'woopack' ); ?>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php submit_button(); ?>

<?php } ?>
