[wpbb-if post:featured_image]
<div class="woopack-product-image">
    [wpbb post:woocommerce_sale_flash]
	[wpbb post:featured_image size="large" display="tag" linked="yes"]
	[wpbb post:woopack_quick_view_button icon='fas fa-eye' text='Quick View']
</div>
[/wpbb-if]

<div class="woopack-product-content">

    <h3 class="woopack-product-title">[wpbb post:link text="title"]</h3>
    
    <div class="woopack-product-rating">
        [wpbb post:woopack_product_rating show_count='yes' text_singular='customer review' text_plural='customer reviews']
    </div>
    
    <div class="woopack-product-price">
        [wpbb post:woocommerce_product_price]
    </div>
    
    [wpbb post:woopack_add_to_cart_button qty_input='before_button' variation_fields='no']

    <div class="woopack-product-meta">
    	Category: 
		[wpbb post:terms_list taxonomy="product_cat" separator=", "]
    </div>
</div>
