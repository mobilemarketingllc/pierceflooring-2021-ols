.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-action .variations_form .label {
	color: inherit;
}
.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-action .variations_form .label label {
	font-size: 12px;
}

.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-action .variations_form .reset_variations {
	margin-left: 5px;
}

.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-action .variations_form .woocommerce-variation-price {
	margin-top: 5px;
}
.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-action .variations_form .woocommerce-variation-price .price {
	font-size: 14px;
}

<?php if ( isset( $settings->variation_table_style ) && 'yes' == $settings->variation_table_style ) { ?>
	.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-action .variations_form table {
		<?php if ( is_numeric( $settings->variation_table_border ) ) { ?>
			<?php WooPack_Helper::print_css( 'border-width', $settings->variation_table_border, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-style', $settings->variation_table_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-color', $settings->variation_table_border_color ); ?>
		<?php } ?>
		<?php WooPack_Helper::print_css( 'width', '100', '%', 'full' == $settings->variation_table_width ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-action .variations_form table tr {
		<?php if ( is_numeric( $settings->variation_table_border ) ) { ?>
			<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->variation_table_border, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-bottom-style', $settings->variation_table_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-bottom-color', $settings->variation_table_border_color ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-action .variations_form table tr > td {
		display: table-cell;
		vertical-align: middle;
		<?php WooPack_Helper::print_css( 'padding', $settings->variation_table_cell_padding, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-action .variations_form table tr > td.label {
		<?php WooPack_Helper::print_css( 'color', $settings->variation_table_label_text_color ); ?>
		<?php WooPack_Helper::print_css( 'background-color', $settings->variation_table_label_bg_color ); ?>
		<?php if ( is_numeric( $settings->variation_table_border ) ) { ?>
			<?php WooPack_Helper::print_css( 'border-right-width', $settings->variation_table_border, 'px' ); ?>
			<?php WooPack_Helper::print_css( 'border-right-style', $settings->variation_table_border_style ); ?>
			<?php WooPack_Helper::print_css( 'border-right-color', $settings->variation_table_border_color ); ?>
		<?php } ?>
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-action .variations_form table tr > td.label label {
		margin-bottom: 0px;
	}
	.fl-node-<?php echo $id; ?> .woocommerce .woopack-product-action .variations_form table tr > td.value {
		text-align: left;
		<?php WooPack_Helper::print_css( 'color', $settings->variation_table_value_text_color ); ?>
		<?php WooPack_Helper::print_css( 'background-color', $settings->variation_table_value_bg_color ); ?>
	}
<?php } ?>