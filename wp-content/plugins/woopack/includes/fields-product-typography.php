<?php
/**
 * Style option for products.
 *
 * @since 1.0.0
 */
function filter_product_typography_settings( $settings ) {
	// Handle old Product Title Typography setting.
	$settings = WooPack_Fields::handle_typography_field( $settings, array(
		'product_title_font'   				=> array(
			'type'          					=> 'font'
		),
		'product_title_font_size_custom'	=> array(
			'type'          		=> 'font_size',
			'condition'     		=> ( isset( $settings->product_title_font_size ) && 'custom' == $settings->product_title_font_size )
		),
		'product_title_line_height' 		=> array(
			'type'          					=> 'line_height',
		),
		'product_title_text_transform' 		=> array(
			'type'          					=> 'text_transform',
			'condition'     					=> ( isset( $settings->product_title_text_transform ) && 'default' != $settings->product_title_text_transform )
		)
	), 'product_title_typography' );

	// Handle old Regular Price Typography setting.
	$settings = WooPack_Fields::handle_typography_field( $settings, array(
		'regular_price_font'   		=> array(
			'type'          					=> 'font'
		),
		'regular_price_font_size'	=> array(
			'type'          			=> 'font_size'
		),
		'regular_price_line_height'	=> array(
			'type'          					=> 'line_height',
		)
	), 'regular_price_typography' );

	// Handle old Sale Price Typography setting.
	$settings = WooPack_Fields::handle_typography_field( $settings, array(
		'sale_price_font_size'		=> array(
			'type'          			=> 'font_size',
		),
		'sale_price_line_height'	=> array(
			'type'          			=> 'line_height',
		)
	), 'sale_price_typography' );

	// Handle old Short Description Typography setting.
	$settings = WooPack_Fields::handle_typography_field( $settings, array(
		'short_description_font'   				=> array(
			'type'          						=> 'font'
		),
		'short_description_font_size_custom'	=> array(
			'type'          						=> 'font_size',
			'condition'     						=> ( isset( $settings->short_description_font_size ) && 'custom' == $settings->short_description_font_size )
		),
		'short_description_line_height'			=> array(
			'type'          						=> 'line_height',
		)
	), 'short_description_typography' );

	// Handle old Sale Badge Typography setting.
	$settings = WooPack_Fields::handle_typography_field( $settings, array(
		'sale_badge_font'   			=> array(
			'type'          				=> 'font'
		),
		'sale_badge_font_size_custom'	=> array(
			'type'          				=> 'font_size',
			'condition'     				=> ( isset( $settings->sale_badge_font_size ) && 'custom' == $settings->sale_badge_font_size )
		),
		'sale_badge_line_height'		=> array(
			'type'          				=> 'line_height',
		)
	), 'sale_badge_typography' );

	// Handle old Out of Stock Typography setting.
	$settings = WooPack_Fields::handle_typography_field( $settings, array(
		'out_of_stock_font'   			=> array(
			'type'          				=> 'font'
		),
		'out_of_stock_font_size_custom'	=> array(
			'type'          				=> 'font_size',
			'condition'     				=> ( isset( $settings->out_of_stock_font_size ) && 'custom' == $settings->out_of_stock_font_size )
		),
		'out_of_stock_line_height'		=> array(
			'type'          				=> 'line_height',
		)
	), 'out_of_stock_typography' );

	// Handle old Product Taxonomy Typography setting.
	$settings = WooPack_Fields::handle_typography_field( $settings, array(
		'meta_font'   			=> array(
			'type'          		=> 'font'
		),
		'meta_font_size_custom'	=> array(
			'type'          		=> 'font_size',
			'condition'     		=> ( isset( $settings->meta_font_size ) && 'custom' == $settings->meta_font_size )
		)
	), 'meta_typography' );

	// Handle old Quick View Typography setting.
	$settings = WooPack_Fields::handle_typography_field( $settings, array(
		'quick_view_font'   			=> array(
			'type'          				=> 'font'
		),
		'quick_view_font_size_custom'	=> array(
			'type'          				=> 'font_size',
			'condition'     				=> ( isset( $settings->quick_view_font_size ) && 'custom' == $settings->quick_view_font_size )
		)
	), 'quick_view_typography' );

	// Return the filtered settings.
	return $settings;
}
/**
 * Typography option for products.
 *
 * @since 1.0.0
 */
function woopack_product_typography_fields()
{
	return apply_filters( 'woopack_product_typography_fields', array(
		'product_title_fonts'   => array(
			'title'                 => __('Product Title', 'woopack'),
			'fields'                => array(
				'product_title_heading_tag'     => array(
					'type' 			              => 'select',
					'label' 		              => __('Heading Tag', 'woopack'),
					'default'                     => 'h2',
					'options'                     => array(
						'h1'                        => __('H1', 'woopack'),
						'h2'                        => __('H2', 'woopack'),
						'h3'                        => __('H3', 'woopack'),
						'h4'                        => __('H4', 'woopack'),
						'h5'                        => __('H5', 'woopack'),
						'h6'                        => __('H6', 'woopack'),
					),
				),
				'product_title_typography'		=> array(
					'type'        	    		=> 'typography',
					'label'       	    		=> __( 'Typography', 'woopack' ),
					'responsive'  	    		=> true,
					'preview'           		=> array(
						'type'              		=> 'css',
						'selector' 		    		=> '.woocommerce ul.products li.product .woopack-product-title,
														.woocommerce div.products div.product .woopack-product-title',
					),
				),
				'product_title_color'           => array(
					'type'              			=> 'color',
					'label'             			=> __('Color', 'woopack'),
					'show_reset'        			=> true,
					'preview'           			=> array(
						'type'              			=> 'css',
						'selector'          			=> '.woocommerce ul.products li.product .woopack-product-title, .woocommerce div.products div.product .woopack-product-title',
						'property'          			=> 'color',
					),
				),
				'product_title_color_hover'           => array(
					'type'              			=> 'color',
					'label'             			=> __('Hover Color', 'woopack'),
					'show_reset'        			=> true,
					'preview'           			=> array(
						'type'              			=> 'none',
					),
				),
				'product_title_margin_top'    	=> array(
					'type' 							=> 'unit',
					'label' 						=> __('Margin Top', 'woopack'),
					'default' 						=> '10',
					'units'							=> array('px'),
					'slider'						=> true,
					'responsive' 					=> true,
					'preview'           			=> array(
						'type'              			=> 'css',
						'selector'          			=> '.woocommerce ul.products li.product .woopack-product-title, .woocommerce div.products div.product .woopack-product-title',
						'property'          			=> 'margin-top',
						'unit'							=> 'px'
					),
				),
				'product_title_margin_bottom'	=> array(
					'type' 							=> 'unit',
					'label' 						=> __('Margin Bottom', 'woopack'),
					'default' 						=> '10',
					'units'							=> array('px'),
					'slider'						=> true,
					'responsive' 					=> true,
					'preview'           			=> array(
						'type'              			=> 'css',
						'selector'          			=> '.woocommerce ul.products li.product .woopack-product-title, .woocommerce div.products div.product .woopack-product-title',
						'property'          			=> 'margin-bottom',
						'unit'							=> 'px'
					),
				),
			),
		),
		'product_price_fonts'   => array(
			'title'                 => __('Price', 'woopack'),
			'collapsed'				=> true,
			'fields'                => array(
				'regular_price_typography'		=> array(
					'type'        	    		=> 'typography',
					'label'       	    		=> __( 'Typography', 'woopack' ),
					'responsive'  	    		=> true,
					'preview'           		=> array(
						'type'              		=> 'css',
						'selector' 		    		=> '.woocommerce ul.products li.product .price .amount,
														.woocommerce div.products div.product .price .amount',
					),
				),
				'regular_price_color'			=> array(
					'type'              		=> 'color',
					'label'             		=> __('Color', 'woopack'),
					'show_reset'        		=> true,
					'preview'           		=> array(
						'type'              		=> 'css',
						'selector'          		=> '.woocommerce ul.products li.product .price .amount, .woocommerce div.products div.product .price .amount',
						'property'          		=> 'color',
					),
				),
				'product_price_margin_bottom'	=> array(
					'type' 						=> 'unit',
					'label' 					=> __('Margin Bottom', 'woopack'),
					'default' 					=> '',
					'units'						=> array('px'),
					'slider'					=> true,
					'responsive' 				=> true,
					'preview'					=> array(
						'type' 						=> 'css',
						'selector'					=> '.woocommerce ul.products li.product .price .amount, .woocommerce div.products div.product .price .amount',
						'property'      			=> 'margin-bottom',
						'unit'						=> 'px'
					),
				),
			),
		),
		'sale_price_fonts'      => array(
			'title'                 => __('Sale Price', 'woopack'),
			'collapsed'				=> true,
			'fields'                => array(
				'sale_price_typography'	=> array(
					'type'        	    	=> 'typography',
					'label'       	    	=> __( 'Typography', 'woopack' ),
					'responsive'  	    	=> true,
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector' 		   		=> '.woocommerce ul.products li.product .price ins .amount,
													.woocommerce div.products div.product .price ins .amount',
					),
				),
				'sale_price_color'      => array(
					'type'              	=> 'color',
					'label'             	=> __('Color', 'woopack'),
					'show_reset'        	=> true,
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector'          	=> '.woocommerce ul.products li.product .price ins .amount, .woocommerce div.products div.product .price ins .amount',
						'property'          	=> 'color',
					),
				),
			),
		),
		'short_description'		=> array(
			'title'                 => __('Short Description', 'woopack'),
			'collapsed'				=> true,
			'fields'                => array(
				'short_description_typography'		=> array(
					'type'        	    		=> 'typography',
					'label'       	    		=> __( 'Typography', 'woopack' ),
					'responsive'  	    		=> true,
					'preview'           		=> array(
						'type'              		=> 'css',
						'selector' 		    		=> '.woocommerce ul.products li.product .woopack-product-description,
														.woocommerce div.products div.product .woopack-product-description',
					),
				),
				'short_description_color'           => array(
					'type'              				=> 'color',
					'label'             				=> __('Color', 'woopack'),
					'show_reset'        				=> true,
					'preview'           				=> array(
						'type'              				=> 'css',
						'selector'          				=> '.woocommerce ul.products li.product .woopack-product-description, .woocommerce div.products div.product .woopack-product-description',
						'property'          				=> 'color',
					),
				),
				'product_description_margin_bottom' => array(
					'type' 								=> 'unit',
					'label' 							=> __('Margin Bottom', 'woopack'),
					'default' 							=> '',
					'units'								=> array('px'),
					'slider'							=> true,
					'responsive' 						=> true,
					'preview'           				=> array(
						'type'              				=> 'css',
						'selector'          				=> '.woocommerce ul.products li.product .woopack-product-description, .woocommerce div.products div.product .woopack-product-description',
						'property'          				=> 'margin-bottom',
						'unit'								=> 'px'
					),
				),
			),
		),
		'sale_badge_fonts'      => array(
			'title'             	=> __('Sale Badge', 'woopack'),
			'collapsed'				=> true,
			'fields'            	=> array(
				'sale_badge_typography'		=> array(
					'type'        	    		=> 'typography',
					'label'       	    		=> __( 'Typography', 'woopack' ),
					'responsive'  	    		=> true,
					'preview'           		=> array(
						'type'              		=> 'css',
						'selector' 		    		=> '.woocommerce ul.products li.product .onsale,
														.woocommerce div.products div.product .onsale,
														.woocommerce .product span.onsale',
					),
				),
			),
		),
		'out_of_stock_fonts'    => array(
			'title'             	=> __('Out of Stock', 'woopack'),
			'collapsed'				=> true,
			'fields'            	=> array(
				'out_of_stock_typography'	=> array(
					'type'        	    		=> 'typography',
					'label'       	    		=> __( 'Typography', 'woopack' ),
					'responsive'  	    		=> true,
					'preview'           		=> array(
						'type'              		=> 'css',
						'selector' 		    		=> '.woocommerce .products .product.outofstock .woopack-product-image .woopack-out-of-stock span',
					),
				),
			),
		),
		'meta_fonts'            => array(
			'title'                 => __('Product Taxonomy', 'woopack'),
			'collapsed'				=> true,
			'fields'				=> array(
				'meta_typography'		=> array(
					'type'        	    	=> 'typography',
					'label'       	    	=> __( 'Typography', 'woopack' ),
					'responsive'  	    	=> true,
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector' 		    	=> '.woocommerce ul.products li.product .woopack-product-meta .product_meta,
													.woocommerce div.products div.product .woopack-product-meta .product_meta',
					),
				)
			),
		),
		'rating_count_taxonomy' => array(
			'title'     => __('Rating Count Taxonomy', 'woopack'),
			'collapsed' => true,
			'fields'    => array(
				'rating_count_taxonomy' => array(
					'type'       => 'typography',
					'label'      => __( 'Typography', 'woopack' ),
					'responsive' => true,
					'preview'    => array(
						'type'     => 'css',
						'selector' => '.woocommerce ul.products li.product .woocommerce-rating-count,
 										.woocommerce div.products div.product .woocommerce-rating-count',
					),
				)
			),
		),
		'quick_view_fonts' 	    => array(
			'title'                 => __('Quick View', 'woopack'),
			'collapsed'				=> true,
			'fields'                => array(
				'quick_view_typography'		=> array(
					'type'        	    	=> 'typography',
					'label'       	    	=> __( 'Typography', 'woopack' ),
					'responsive'  	    	=> true,
					'preview'           	=> array(
						'type'              	=> 'css',
						'selector' 		    	=> '.woopack-product-quick-view p',
					),
				),
			),
		),
	) );
}
