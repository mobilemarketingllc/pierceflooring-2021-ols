<?php

class WooPack_Helper {
	static public $order;

	static public $settings = null;

	static private $_product_images = array();

	/**
	 * Get list of modules
	 *
	 * @since 1.1.0
	 * @return array
	 */
	static public function get_modules()
	{
		return array(
			'single-product',
			'add-to-cart',
			'product-grid',
			'product-carousel',
			'cart',
			'checkout',
			'product-category',
			'mini-cart',
			'offcanvas-cart',
			'my-account',
		);
	}

	static public function get_template_file( $filename = '', $settings, $product, $path = false, $args = array() ) {
		if ( empty( $filename ) ) {
			return;
		}

		extract( $args, EXTR_SKIP );

		// Remove extension if provided.
		$filename = str_replace( '.php', '', $filename );
		// Re-add extension.
		$filename = $filename . '.php';

		$theme_dir = '';
		$file = '';

		if ( is_child_theme() ) {
			$theme_dir = get_stylesheet_directory();
		} else {
			$theme_dir = get_template_directory();
		}

		if ( file_exists( $theme_dir . '/woopack/templates/' . $filename ) ) {
			$file = $theme_dir . '/woopack/templates/' . $filename;
		} elseif ( file_exists( WOOPACK_DIR . 'templates/' . $filename ) ) {
			$file = WOOPACK_DIR . 'templates/' . $filename;
		}

		if ( $path ) {
			return $file;
		}

		if ( ! empty( $file ) ) {
			include $file;
		}
	}

	/**
	 * Get admin label from settings.
	 *
	 * @since 1.0.0
	 * @return string $admin_label
	 */ 
	static public function get_admin_label()
	{
		$admin_label = WooPack_Admin_Settings::get_option( 'woopack_admin_label' );
		$admin_label = trim( $admin_label ) !== '' ? trim( $admin_label ) : 'WooPack';

		return $admin_label;
	}

	/**
	 * Get group name for modules from settings.
	 *
	 * @since 1.1.0
	 * @return string $group_name
	 */
	static public function get_modules_group()
	{
		$list_with_standard = WooPack_Admin_Settings::get_option( 'woopack_list_modules_with_standard' );

		if ( $list_with_standard ) {
			return '';
		}

		$group_name = WooPack_Admin_Settings::get_option( 'woopack_builder_label' );
		$group_name = trim( $group_name ) !== '' ? trim( $group_name ) : 'WooPack ' . __('Modules', 'woopack');

		return $group_name;
	}

	/**
	 * Get list of Products
	 *
	 * @since 1.0.0
	 * @return array $options Array of products
	 */
	static public function get_products_list()
	{
		global $wpdb;

		$type = 'product';
		$options = array();

		$posts = $wpdb->get_results( $wpdb->prepare( "
			SELECT ID, post_title FROM {$wpdb->posts}
			WHERE post_type = %s
			AND post_status = 'publish'
			ORDER BY post_date DESC
		", $type ) );

		foreach ( $posts as $post ) {
			$options[$post->ID] = $post->post_title;
		}

		return $options;
	}

	/**
	 * Get All Taxonomies
	 *
	 * @since 1.0.0
	 * @return array $options Array of taxonomies
	 */
	static public function get_taxonomies_list() {
		$options = array();

		$slug = 'product';
		$taxonomies = FLBuilderLoop::taxonomies($slug);

		foreach($taxonomies as $tax_slug => $tax) {
			$options[$tax_slug] = $tax->label;
		}

		return $options;
	}

	/**
	 * Get color value either hex or rgb.
	 *
	 * @since 1.0.0
	 * @return string $color
	 */
	static public function get_color_value( $color )
	{
		if ( $color == '' || ! $color ) {
			return;
		}
		if ( false === strpos( $color, 'rgb' ) ) {
			return '#' . $color;
		} else {
			return $color;
		}
	}

	/**
	* Renders the CSS class for each post item.
	*
	* @since 1.0.0
	* @return void
	*/
	static public function render_post_class( $product, $settings, $layout = 'grid' )
	{
		$show_image = has_post_thumbnail() && $settings->show_image == 'yes';
		$classes    = array( 'woopack-product-' . $layout );
		$classes[] = 'woopack-product-align-' . $settings->product_align;

		if ( $product ) {
			$classes[] = wc_get_loop_class();
			$classes[] = $product->get_stock_status();

			if ( $product->is_on_sale() ) {
				$classes[] = 'sale';
			}
			if ( $product->is_featured() ) {
				$classes[] = 'featured';
			}
			if ( $product->is_downloadable() ) {
				$classes[] = 'downloadable';
			}
			if ( $product->is_virtual() ) {
				$classes[] = 'virtual';
			}
			if ( $product->is_sold_individually() ) {
				$classes[] = 'sold-individually';
			}
			if ( $product->is_taxable() ) {
				$classes[] = 'taxable';
			}
			if ( $product->is_shipping_taxable() ) {
				$classes[] = 'shipping-taxable';
			}
			if ( $product->is_purchasable() ) {
				$classes[] = 'purchasable';
			}
			// if ( $product->get_type() ) {
			// 	$classes[] = "product-type-" . $product->get_type();
			// }
			if ( $product->is_type( 'variable' ) ) {
				if ( ! $product->get_default_attributes() ) {
					$classes[] = 'has-default-attributes';
				}
				if ( $product->has_child() ) {
					$classes[] = 'has-children';
				}
			}
		}

		if ( false !== ( $key = array_search( 'hentry', $classes ) ) ) {
			unset( $classes[ $key ] );
		}

		//post_class( apply_filters( "woopack_product_{$layout}_classes", $classes, $settings ) );
		echo ' class="' . implode( ' ', apply_filters( "woopack_product_{$layout}_classes", array_merge( $classes, get_post_class() ), $settings ) ) . '"';
	}

	/**
	 * Output CSS conditionally.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	static public function print_css( $property, $value, $unit = '', $print = true )
	{
		if ( ! $print || $value == '' ) {
			return;
		}

		$color_properties = array(
			'background-color',
			'background',
			'color',
			'border-color',
			'border-top-color',
			'border-right-color',
			'border-bottom-color',
			'border-left-color',
			'outline-color',
			'fill'
		);

		if ( 'font' == $property ) {
			if ( $value['family'] != 'Default' ) {
				FLBuilderFonts::font_css( $value );
			}
		}
		elseif ( in_array( $property, $color_properties ) ) {
			$color_value = self::get_color_value( $value );
			echo "{$property}: {$color_value}{$unit};\n"; // Here $unit maybe !important
		}
		else {
			echo "{$property}: {$value}{$unit};\n";
		}
	}

	/**
	 * Renders scripts and styles enqueued by shortcodes or widgets.
	 *
	 * @since 1.7
	 * @access private
	 * @return string
	 */
	static public function render_scripts_and_styles( $template_id ) {
		global $wp_scripts;
		global $wp_styles;

		// Running these isn't necessary and can cause performance issues.
		remove_action( 'wp_enqueue_scripts', 'FLBuilder::register_layout_styles_scripts' );
		remove_action( 'wp_enqueue_scripts', 'FLBuilder::enqueue_ui_styles_scripts' );
		remove_action( 'wp_enqueue_scripts', 'FLBuilder::enqueue_all_layouts_styles_scripts' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );

		if ( isset( $wp_styles ) ) {
			$wp_styles->queue = array();
		}
		if ( isset( $wp_scripts ) ) {
			$wp_scripts->queue = array();
		}

		if ( is_callable( 'WC_Frontend_Scripts::load_scripts' ) ) {
			WC_Frontend_Scripts::load_scripts();
		}

		if ( is_callable( 'FLBuilder::enqueue_layout_styles_scripts_by_id' ) ) {
			FLBuilder::enqueue_layout_styles_scripts_by_id( $template_id );
		}
	}

	static public function render_template( $product_id, $template_id = false ) {
		if ( ! empty( $template_id ) ) {
			echo do_shortcode('[fl_builder_insert_layout id="' . $template_id . '"]');
		} else {
			echo do_shortcode('[product_page id="' . $product_id . '"]');
		}
	}

	/**
	 * Build pagination.
	 *
	 * @since 1.1.0
	 * @return void
	 */
	static public function pagination( $query, $current_url = '', $paged = 1 ) {
		$total_pages = $query->max_num_pages;
		$permalink_structure = get_option( 'permalink_structure' );
		$current_url = empty( $current_url ) ? get_pagenum_link() : $current_url;
		$base = untrailingslashit( html_entity_decode( $current_url ) );

		if ( $total_pages > 1 ) {

			if ( ! $current_page = $paged ) { // @codingStandardsIgnoreLine
				$current_page = 1;
			}

			$base = FLBuilderLoop::build_base_url( $permalink_structure, $base );

			echo paginate_links(array(
				'base'	   => $base . '%_%',
				'format'   => '/#page-%#%',
				'current'  => $current_page,
				'total'	   => $total_pages,
				'type'	   => 'list',
			));
		}
	}

	static public function featured_products($query_args)
	{
		$query_args['tax_query'][] = array(
			'taxonomy'         => 'product_visibility',
			'terms'            => 'featured',
			'field'            => 'name',
			'operator'         => 'IN',
			'include_children' => false,
		);

		return $query_args;
	}

	static public function best_selling_products( $query_args )
	{
		$query_args['meta_key'] = 'total_sales';
		$query_args['order']    = 'DESC';
		$query_args['orderby']  = 'meta_value_num';
		
		return $query_args;
	}

	static public function sale_products( $query_args )
	{
		$final_ids = wc_get_product_ids_on_sale();
		if ( isset( $query_args['post__not_in'] ) && is_array( $query_args['post__not_in'] ) ) {
			$final_ids = array_diff( $final_ids, $query_args['post__not_in'] );
		}
		if ( isset( $query_args['post__in'] ) && is_array( $query_args['post__in'] ) ) {
			$query_args['post__in'] = array_merge( $query_args['post__in'], $final_ids );
		} else {
			$query_args['post__in'] = array_merge( array( 0 ), $final_ids );
		}

		return $query_args;
	}

	static public function top_rated_products( $query_args )
	{
		$query_args['meta_key'] = '_wc_average_rating';
		$query_args['order']    = 'DESC';
		$query_args['orderby']  = 'meta_value_num';
		
		return $query_args;
	}

	static public function related_products( $query_args ) {
		if ( ! function_exists( 'wc_get_related_products' ) ) {
			return $query_args;
		}

		$limit = 4;

		if ( is_object( self::$settings ) && isset( $settings->posts_per_page ) ) {
			$limit = ! empty( $settings->posts_per_page ) ? $settings->posts_per_page : $limit;
		}

		$related_products = wc_get_related_products( get_the_ID(), $limit );

		if ( ! isset( $query_args['post__in'] ) || ! is_array( $query_args['post__in'] ) ) {
			$query_args['post__in'] = $related_products;
		} else {
			$query_args['post__in'] = array_diff( $related_products, $query_args['post__in'] );
		}

		return $query_args;
	}

	static public function order_by_menu_order( $query ) {
	}

	static public function order_by_rand( $query )
	{
		if ( $query->get('fl_builder_loop') ) {
			$query->set('orderby', 'rand');
		}
	}

	static public function order_by_date( $query )
	{
		if ( $query->get('fl_builder_loop') ) {
			$query->set('orderby', 'date ID');
			$query->set('order', ( 'asc' === self::$order ) ? 'ASC' : 'DESC');
		}
	}

	static public function order_by_price( $query )
	{
		if ( $query->get('fl_builder_loop') ) {
			$query->set('meta_key', '_price');
			$query->set('orderby', 'meta_value_num');
			if ( self::$order == 'desc' ) {
				$query->set('order', 'DESC');
			} else {
				$query->set('order', 'ASC');
			}
		}
	}

	static public function order_by_rating( $query )
	{
		if ( $query->get('fl_builder_loop') ) {
			$query->set('meta_key', '_wc_average_rating');
			$query->set('orderby', 'meta_value_num');
			$query->set('order', 'DESC');
		}
	}

	static public function order_by_popularity( $query )
	{
		if ( $query->get('fl_builder_loop') ) {
			$query->set('meta_key', 'total_sales');
			$query->set('orderby', 'meta_value_num');
			$query->set('order', 'DESC');
		}
	}

	static public function exclude_post( $args ) {
		if ( ! isset( $args['settings'] ) ) {
			return $args;
		}

		$settings = $args['settings'];

		if ( ! isset( $settings->woopack ) || ! $settings->woopack ) {
			return $args;
		}

		if ( ! isset( $settings->exclude_current ) || 'no' === $settings->exclude_current ) {
			return $args;
		}

		if ( ! isset( $settings->woopack_post_id ) ) {
			return $args;
		}

		if ( isset( $args['post__in'] ) && is_array( $args['post__in'] ) ) {
			$args['post__in'] = array_diff( $args['post__in'], array( $settings->woopack_post_id ) );
		}
		
		if ( ! isset( $args['post__not_in'] ) || ! is_array( $args['post__not_in'] ) ) {
			$args['post__not_in'] = array();
		}

		$args['post__not_in'][] = $settings->woopack_post_id;

		return $args;
	}

	static public function get_fl_theme_product_singular_layouts( $as_options = false ) {
		$args = array(
			'post_type'	=> 'fl-theme-layout',
			'post_status'	=> 'publish',
			'posts_per_page' => '-1',
			'meta_query'	=> array(
				'relation'	=> 'AND',
				array(
					'key'	=> '_fl_theme_layout_type',
					'value' => 'part',
					'compare' => '='
				),
				array(
					'key'	=> '_fl_theme_layout_hook',
					'value' => 'woopack_quick_view_template',
					'compare' => '='
				),
				array(
					'key'	=> '_fl_theme_builder_locations',
					'value' => 'post:product',
					'compare' => 'REGEXP'
				),
			),
		);

		$posts = get_posts( $args );

		if ( $as_options ) {
			$options = array(
				''	=> __( 'Default', 'woopack' ),
			);
			if ( empty( $posts ) ) {
				return $options;
			}
			foreach ( $posts as $post ) {
				$title = get_the_title( $post->ID ) . ' (Themer Layout)';
				$options[ $post->ID ] = $title;
			}
			return $options;
		}

		return $posts;
	}

	static public function render_product_images( $product, $settings ) {
		$product_id = $product->get_id();
		$image = '';

		if ( method_exists( $product, 'get_image' ) ) {
			$image = $product->get_image( $settings->image_size, array( 'data-no-lazy' => '1', 'class' => 'woopack-product-featured-image' ) );
		} else {
			$image = get_the_post_thumbnail( $product_id, $settings->image_size, array( 'data-no-lazy' => '1', 'class' => 'woopack-product-featured-image' ) );
		}
		?>
		<div class="woopack-product-image-wrap" style="display: none;"><?php //echo $image; ?></div>
		<div class="woopack-product-images<?php echo isset( $settings->image_slider ) && 'yes' === $settings->image_slider ? ' woopack-has-slider' : ''; ?>">
			<div class="woopack-product-image-slide"><?php echo $image; ?></div>
			<?php
			if ( isset( $settings->image_slider ) && 'yes' === $settings->image_slider && method_exists( $product, 'get_gallery_image_ids' ) ) {
				$gallery_images = $product->get_gallery_image_ids();
				if ( ! empty( $gallery_images ) ) {
					foreach ( $gallery_images as $image_id ) {
						$image = wp_get_attachment_image( $image_id, $settings->image_size, false, array( 'data-no-lazy' => '1', 'class' => 'woopack-product-gallery-image' ) );
						if ( empty( trim( $image ) ) ) {
							continue;
						}
						?>
						<div class="woopack-product-image-slide">
						<?php echo $image; ?>
						</div>
						<?php
					}
				}
			}
			?>
		</div>
		<?php
	}
}
