<?php
	$responsive_breakpoint = empty( $global_settings->responsive_breakpoint ) ? 767 : $global_settings->responsive_breakpoint;
?>
;(function($){
		function toggleNavOnMobile() {
			if ( window.innerWidth <= <?php echo $responsive_breakpoint; ?> ) {
				$('.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce-MyAccount-navigation').on('click', function(e) {
					$(this).toggleClass( 'expanded' );

					if ( $( e.target ).parent().hasClass( 'is-active' ) ) {
						e.preventDefault();
						e.stopPropagation();
					}
				});
			}
		}

		$(document).ready(function(){

			$('.fl-node-<?php echo $id; ?> button[name="save_address"], .fl-node-<?php echo $id; ?> button[name="save_account_details"]').parent().addClass('woopack-my-account-button');

		    // Select and loop the container element of the elements you want to equalise
		    $('.fl-node-<?php echo $id; ?> #customer_login').each(function(){

		      // Cache the highest
		      var highestBox = 0;

		      // Select and loop the elements you want to equalise
		      $('form', this).each(function(){

		        // If this box is higher than the cached highest then store it
		        if($(this).height() > highestBox) {
		          highestBox = $(this).height();
		        }
		      });
			  
		      // Set the height of all those children to whichever was highest
		      $('form',this).height(highestBox);

		    });

			// Toggle Nav in responsive devices
			toggleNavOnMobile();
		});

})(jQuery);
