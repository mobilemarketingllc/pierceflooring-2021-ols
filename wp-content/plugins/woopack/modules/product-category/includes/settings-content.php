<div class="fl-custom-query fl-loop-data-source" data-source="custom_query">
	<div id="fl-builder-settings-section-filter" class="fl-builder-settings-section">
		<table class="fl-form-table fl-custom-query-filter fl-custom-query-product-filter" style="display:table;" >
			<?php
			$taxonomies = FLBuilderLoop::taxonomies('product');
			foreach($taxonomies as $tax_slug => $tax) {
				if ( 'product_cat' == $tax->name ) {
					FLBuilder::render_settings_field( 'tax_product' . '_' . $tax_slug, array(
						'type'          => 'suggest',
						'action'        => 'fl_as_terms',
						'data'          => $tax_slug,
						'label'         => $tax->label,
						'help'          => sprintf( __( 'Enter a list of %1$s.', 'woopack' ), $tax->label ),
						'matching'      => true
					), $settings );
				}
			}
			?>
		</table>

		<table class='fl-form-table'>
			<?php
				FLBuilder::render_settings_field(
					'order_by',
					array(
						'type'    => 'select',
						'label'   => __( 'Order By', 'woopack' ),
						'default' => 'name',
						'options' => array(
							'' 			=> __( 'Default', 'woopack' ),
							'term_id'	=> __( 'ID', 'woopack' ),
							'name'   => __( 'Name', 'woopack' ),
							'slug'   => __( 'Slug', 'woopack' ),
							'parent' => __( 'Parent', 'woopack' ),
							'count'  => __( 'Post Count', 'woopack' ),
						),
					),
					$settings
				);
				?>
		</table>

		<table class='fl-form-table'>
		<?php
				FLBuilder::render_settings_field(
					'order',
					array(
						'type'    => 'select',
						'label'   => __( 'Order', 'woopack' ),
						'default' => 'ASC',
						'options' => array(
							'ASC'  => __( 'Ascending', 'woopack' ),
							'DESC' => __( 'Descending', 'woopack' ),
						),
					),
					$settings
				);

				?>
		</table>

		<table class='fl-form-table'>
			<?php

				FLBuilder::render_settings_field(
					'display_data',
					array(
						'type'    => 'select',
						'label'   => __( 'Display', 'bb-powerpack' ),
						'default' => 'default',
						'options' => array(
							'default'       => __( 'Default', 'bb-powerpack' ),
							'parent_only'   => __( 'Parent Only', 'bb-powerpack' ),
							'children_only' => __( 'Children Only', 'bb-powerpack' ),
						),
					),
					$settings
				);

				FLBuilder::render_settings_field(
					'on_tax_archive',
					array(
						'type'    => 'select',
						'label'   => __( 'On Taxonomy Archive', 'bb-powerpack' ),
						'default' => isset( $settings->on_tax_archive ) ? $settings->on_tax_archive : 'default',
						'options' => array(
							'default'       => __( 'Default', 'bb-powerpack' ),
							'parent_only' => __( 'Parent of Current Category', 'bb-powerpack' ),
							'children_only' => __( 'Children of Current Category', 'bb-powerpack' ),
						),
					),
					$settings
				);
				?>
		</table>
	</div>
</div>