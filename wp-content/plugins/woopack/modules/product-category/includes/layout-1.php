<div class="woopack-product-category woopack-clear" title="<?php echo $cat->name; ?>">
	<?php
		$style       = $settings->category_style;
		$category_id = $cat->term_id;
		$a_attrs 	 = 'href="' . $term_link . '" target="' . $settings->category_link_target . '"';
		if ( '_blank' == $settings->category_link_target ) {
			$a_attrs .= ' rel="nofollow noopener"';
		}
		$a_tag_start = '<a ' . $a_attrs . ' class="woopack-product-category__link">';
		$a_tag_end   = '</a>';
	?>
		<div class="product-category-inner product-category-<?php echo $style; ?>">
			<?php echo 'box' === $settings->category_link_window ? $a_tag_start : ''; ?>
				<div class="woopack-product-category__img">
					<?php if ( is_array( $shop_catalog_img ) && ! empty( $shop_catalog_img ) ) { ?>
						<img src="<?php echo $shop_catalog_img[0]; ?>" alt="<?php echo $cat->name; ?>">
						<?php } else { ?>
							<img src="<?php echo WOOPACK_URL; ?>assets/images/placeholder-300.jpg" alt="<?php echo $cat->name; ?>">
					<?php } ?>
				</div>
				<div class="woopack-product-category__content">
					<div class='woopack-product-category__title_wrapper'>
						<?php echo ( 'title' === $settings->category_link_window ) || ( 'title_button' === $settings->category_link_window ) ? $a_tag_start : ''; ?>
							<?php if ( 'style-0' == $style ) { ?>
								<<?php echo $settings->category_title_tag; ?> class="woopack-product-category__title">
									<?php echo $cat->name; ?>
								</<?php echo $settings->category_title_tag; ?>>
								<?php if ( 'yes' === $settings->category_show_counter ) { ?>
									<span class="count"><?php echo $cat->count; ?> <?php echo $settings->category_count_text; ?> </span>
								<?php }; ?>
							<?php } else { ?>
								<<?php echo $settings->category_title_tag; ?> class="woopack-product-category__title">
									<?php echo $cat->name; ?>
									<?php if ( 'yes' === $settings->category_show_counter ) { ?>
										<span class="count">( <?php echo $cat->count; ?> )</span>
									<?php }; ?>
								</<?php echo $settings->category_title_tag; ?>>
							<?php } ?>
						<?php echo ( 'title' === $settings->category_link_window ) || ( 'title_button' === $settings->category_link_window ) ? $a_tag_end : ''; ?>
					</div>
					<?php if ( isset( $settings->category_show_desc ) && 'yes' === $settings->category_show_desc ) : ?>
					<div class='woopack-product-category__description_wrapper'>
						<p class="woopack-product-category__description"><?php echo $cat->category_description; ?></p>
					</div>
					<?php endif; ?>	
					<?php if ( 'yes' === $settings->category_show_button ) : ?>
						<div class="woopack-product-category__button_wrapper">
							<?php echo ( 'button' === $settings->category_link_window ) || ( 'title_button' === $settings->category_link_window ) ? $a_tag_start : ''; ?>
								<button type="button" name="button" class="woopack-product-category__button">
									<?php
									if ( '' !== $settings->category_button_text ) {
										echo $settings->category_button_text;
									} else {
										esc_html_e( 'Shop Now', 'woopack' );
									}
									?>
								</button>
							<?php echo ( 'button' === $settings->category_link_window ) || ( 'title_button' === $settings->category_link_window ) ? $a_tag_end : ''; ?>
						</div>
					<?php endif; ?>
				</div>
			<?php echo 'box' === $settings->category_link_window ? $a_tag_end : ''; ?>
		</div>
</div>
