<?php
if ( isset( $settings->product_id ) && $settings->product_id != '' ) {

	global $post, $product;

    $product_id 	= $settings->product_id;
	$product 		= wc_get_product( $product_id );
	
	if ( $product ) :

		$product_data 	= $product->get_data();
		$image_size 	= $settings->image_size;
		$image 			= wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), $image_size );
		$attachment_ids = $product_data['gallery_image_ids'];

		$post = get_post( $product_id, OBJECT );
		setup_postdata( $post );

		do_action( 'woopack_before_product' );
		?>

		<div id="woopack-product-<?php echo $product_id; ?>" class="woopack-single-product woocommerce single-product-layout-<?php echo $settings->product_layout; ?> clearfix">

			<div class="summary entry-summary clearfix">

			<?php
				include apply_filters( 'woopack_single_product_layout_path', $module->dir . 'includes/layout-' . $settings->product_layout . '.php', $settings );
			?>

			</div> <!-- summary -->
		</div> <!-- woopack-single-product -->

		<?php
		do_action( 'woopack_after_product' );

		wp_reset_postdata();
		
	endif;
}
?>
