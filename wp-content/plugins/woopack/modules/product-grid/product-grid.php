<?php
/**
* @class WooPackProductGrid
*/
class WooPackProductGrid extends FLBuilderModule {
    /**
    * Constructor function for the module. You must pass the
    * name, description, dir and url in an array to the parent class.
    *
    * @method __construct
    */
    public function __construct()
    {
        parent::__construct(array(
            'name' 				=> __('Product Grid', 'woopack'),
            'description' 		=> __('Addon to display Product Grid.', 'woopack'),
            'group'             => WooPack_Helper::get_modules_group(),
            'category' 			=> WOOPACK_CAT,
            'dir' 				=> WOOPACK_DIR . 'modules/product-grid/',
            'url' 				=> WOOPACK_URL . 'modules/product-grid/',
            'editor_export' 	=> true, // Defaults to true and can be omitted.
            'enabled' 			=> true, // Defaults to true and can be omitted.
		));
		
		add_filter( 'fl_builder_loop_query_args', array( $this, 'exclude_current_post' ), 10, 1 );
	}

    /**
    * @method enqueue_scripts
    */
    public function enqueue_scripts()
    {
		$this->add_css( 'font-awesome-5' );
        $this->add_js('imagesloaded');
        $this->add_js('jquery-isotope', WOOPACK_URL . 'assets/js/isotope.pkgd.min.js', array('jquery'), WOOPACK_VER, false);

        if (FLBuilderModel::is_builder_active() || $this->settings->pagination == 'scroll') {
            $this->add_js('jquery-infinitescroll');
		}

		if ( isset( $this->settings->image_slider ) && 'yes' === $this->settings->image_slider && ! FLBuilderModel::is_builder_active() ) {
			$this->add_css( 'woopack-slick-theme', WOOPACK_URL . 'assets/slick/slick-theme.css', array(), '1.8.0', false );
			$this->add_css( 'woopack-slick', WOOPACK_URL . 'assets/slick/slick.css', array(), '1.8.0', false );
			$this->add_js( 'woopack-slick', WOOPACK_URL . 'assets/slick/slick.js', array('jquery'), '1.8.0', true );
		}

        $this->add_css( 'woopack-modal-box' );
		$this->add_js( 'woopack-modal-box' );

        // Jetpack sharing has settings to enable sharing on posts, post types and pages.
        // If pages are disabled then jetpack will still show the share button in this module
        // but will *not* enqueue its scripts and fonts.
        // This filter forces jetpack to enqueue the sharing scripts.
        add_filter( 'sharing_enqueue_scripts', '__return_true' );
	}


    public function get_layout()
    {
        //$layout = $this->layout;
        return 1;
	}
	
	public function get_layout_style() {
		return 'grid';
	}

	public function get_item_html_tag() {
		return 'li';
	}

	public function update( $settings ) {
		$settings->item_html_tag = $this->get_item_html_tag();
		$settings->layout_style = $this->get_layout_style();
		return $settings;
	}

    /**
    * Renders the CSS class for each post item.
    *
    * @since 1.0.0
    * @return void
    */
    public function render_post_class( $product )
    {
        $settings   = $this->settings;
        $layout     = 'grid';
        $show_image = has_post_thumbnail() && $settings->show_image;
        $classes    = array( 'woopack-product-grid' );
        $classes[] = 'woopack-product-align-' . $settings->product_align;

        if ( $product ) {
            $classes[] = 'product';
            $classes[] = wc_get_loop_class();
            $classes[] = $product->get_stock_status();

            if ( $product->is_on_sale() ) {
                $classes[] = 'sale';
            }
            if ( $product->is_featured() ) {
                $classes[] = 'featured';
            }
            if ( $product->is_downloadable() ) {
                $classes[] = 'downloadable';
            }
            if ( $product->is_virtual() ) {
                $classes[] = 'virtual';
            }
            if ( $product->is_sold_individually() ) {
                $classes[] = 'sold-individually';
            }
            if ( $product->is_taxable() ) {
                $classes[] = 'taxable';
            }
            if ( $product->is_shipping_taxable() ) {
                $classes[] = 'shipping-taxable';
            }
            if ( $product->is_purchasable() ) {
                $classes[] = 'purchasable';
            }
            if ( $product->get_type() ) {
                $classes[] = "product-type-" . $product->get_type();
            }
            if ( $product->is_type( 'variable' ) ) {
                if ( ! $product->get_default_attributes() ) {
                    $classes[] = 'has-default-attributes';
                }
                if ( $product->has_child() ) {
                    $classes[] = 'has-children';
                }
            }
        }

        if ( false !== ( $key = array_search( 'hentry', $classes ) ) ) {
            unset( $classes[ $key ] );
        }

        post_class( apply_filters( 'woopack_product_grid_classes', $classes, $settings ) );
    }

    /**
     * Renders the product featured image.
     *
     * @since 1.0.0
     * @return void
     */
    public function render_featured_image()
    {
        $settings = $this->settings;

        if ( 'yes' == $settings->show_image && has_post_thumbnail() ) {
            global $product;
            include WOOPACK_DIR . 'templates/loop-product-image.php';
        }
    }

    /**
     * Renders button.
     *
     * @since 1.0.0
     * @return void
     */
    public function render_button()
    {
        $settings = $this->settings;

        if ( 'none' != $settings->button_type ) {
            include WOOPACK_DIR . 'templates/loop-product-button.php';
        }
    }

    /**
     * Renders meta.
     *
     * @since 1.0.0
     * @return void
     */
    public function render_meta()
    {
        $settings = $this->settings;

        if ( 'yes' == $settings->show_taxonomy ) {
            global $product;
            include WOOPACK_DIR . 'templates/loop-product-meta.php';
        }
    }

    /**
    * Checks to see if a featured image exists for a position.
    *
    * @since 1.0.0
    * @param string|array $position
    * @return void
    */
    public function has_featured_image()
    {
        $settings = $this->settings;
        $result   = false;

        if ( has_post_thumbnail() && $settings->show_image ) {
            $result = true;
        }

        return $result;
	}

	public function exclude_current_post( $args ) {
		return WooPack_Helper::exclude_post( $args );
	}

	public function filter_settings( $settings, $helper ) {
		// Handle old Button Settings.
		$settings = filter_product_button_settings( $settings );

		// Handle old Style Settings.
		$settings = filter_product_style_settings( $settings );

		// Handle old Typography Settings.
		$settings = filter_product_typography_settings( $settings );

		// Handle old Filter Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'filter_font'   			=> array(
				'type'          			=> 'font'
			),
			'filter_font_size_custom'	=> array(
				'type'          			=> 'font_size',
				'condition'     			=> ( isset( $settings->filter_font_size ) && 'custom' == $settings->filter_font_size )
			),
			'filter_line_height_custom'	=> array(
				'type'          			=> 'line_height',
				'condition'     			=> ( isset( $settings->filter_line_height ) && 'custom' == $settings->filter_line_height )
			),
		), 'filter_typography' );

		// Handle old Pagination Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'pagination_font'   			=> array(
				'type'          			=> 'font'
			),
			'pagination_font_size_custom'	=> array(
				'type'          			=> 'font_size',
				'condition'     			=> ( isset( $settings->pagination_font_size ) && 'custom' == $settings->pagination_font_size )
			),
		), 'pagination_typography' );

		// Handle old Pagination Border setting.
        $settings = WooPack_Fields::handle_border_field( $settings, array(
            'pagination_border_style'	=> array(
                'type'                  	=> 'style'
            ),
            'pagination_border_width'	=> array(
                'type'						=> 'width'
            ),
            'pagination_border_color'	=> array(
                'type'						=> 'color'
            ),
            'pagination_border_radius'	=> array(
                'type'						=> 'radius'
            )
		), 'pagination_border' );
		return $settings;
	}
}

/**
* Register the module and its form settings.
*/
FLBuilder::register_module('WooPackProductGrid', array(
    'general'   => array(
        'title'     => __('Layout', 'woopack'),
        'sections'  => array(
            'layout'    => array(
                'title'     => __( 'Layout', 'woopack' ),
                'fields'    => array(
                    'product_layout'    => array(
                        'type'              => 'layout',
                        'label'             => '',
                        'default'           => 1,
                        'options'           => array(
                            1                    => WOOPACK_URL . 'modules/product-grid/images/layout-1.png',
                            2                    => WOOPACK_URL . 'modules/product-grid/images/layout-2.png',
                            3                    => WOOPACK_URL . 'modules/product-grid/images/layout-3.png',
                            4                    => WOOPACK_URL . 'modules/product-grid/images/layout-4.png',
                        ),
                    ),
                ),
            ),
            'config'    => array(
				'title'     => __('Structure', 'woopack'),
				'collapsed'	=> false,
                'fields'    => array(
                    'posts_per_page'    => array(
                        'type'            => 'unit',
                        'label'           => __('Posts Per Page', 'woopack'),
                        'default'         => '10',
                        'slider'		  => true
                    ),
                    'product_columns'   => array(
                        'type'              => 'unit',
                        'label'             => __('Columns', 'woopack'),
						'default' 		    => '3',
						'slider'			=> true,
                        'responsive'        => array(
                            'default'       => array(
                                'default' 		    => '3',
                                'medium' 			=> '2',
                                'responsive'		=> '1',
                            ),
                        ),
                    ),
                    'product_spacing'   => array(
                        'type'              => 'unit',
                        'label'             => __('Spacing', 'woopack'),
                        'default'           => '2',
                        'units'				=> array('%'),
						'slider'			=> true,
                        'responsive'        => true,
                    ),
                    'product_align'     => array(
                        'type'              => 'select',
                        'label'             => __('Alignment', 'woopack'),
                        'default'           => 'default',
                        'options'           => array(
                            'default'           => __('Default', 'woopack'),
                            'left'              => __('Left', 'woopack'),
                            'center'            => __('Center', 'woopack'),
                            'right'             => __('Right', 'woopack')
                        ),
                    ),
                    'match_height'      => array(
                        'type'              => 'select',
                        'label'             => __('Equal Heights', 'woopack'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'                => __('No', 'woopack'),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'content'   => array(
        'title'     => __('Content', 'woopack'),
        'file'      => WOOPACK_DIR . 'includes/loop-settings.php',
    ),
    'filter'    => array(
        'title'     => __('Filter', 'bb-powerpack'),
        'sections'  => array(
            'tax_filters'   	=> array(
                'title'         => '',
                'fields'        => array(
                    'enable_filter' => array(
                        'type'          => 'select',
                        'label'         => __('Enable Filters', 'woopack'),
                        'default'       => 'no',
                        'options'       => array(
                            'yes'           => __('Yes', 'woopack'),
                            'no'            => __('No', 'woopack')
                        ),
                        'toggle'        => array(
                            'yes'           => array(
                                'sections'      => array('filter_settings', 'filter_style', 'filter_typography')
                            )
                        )
                    ),
                ),
            ),
            'filter_settings'   => array(
				'title'             => __('Filter Settings', 'bb-powerpack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'filter_taxonomy'   => array(
                        'type'              => 'select',
                        'label'             => __('Select Taxonomy', 'woopack'),
                        'options'           => WooPack_Helper::get_taxonomies_list()
                    ),
                    'filter_type'       => array(
                        'type'              => 'select',
                        'label'             => __('Type', 'woopack'),
                        'default'           => 'static',
                        'options'           => array(
                            'static'            => __('Static', 'woopack'),
                            'dynamic'           => __('Dynamic', 'woopack')
                        )
                    ),
                    'filter_all_label'  => array(
                        'type'              => 'text',
                        'label'             => __('"All" Filter Label', 'woopack'),
                        'default'           => __('All', 'woopack'),
                    ),
                )
            ),
            'filter_style'  	=> array(
				'title'         => __('Filter Style', 'bb-powerpack'),
				'collapsed'		=> true,
                'fields'        => array(
					'filter_position' => array(
						'type'	=> 'select',
						'label'	=> __( 'Position', 'woopack' ),
						'default' => 'top',
						'options' => array(
							'top'	=> __( 'Top', 'woopack' ),
							'left'	=> __( 'Left', 'woopack' ),
							'right'	=> __( 'Right', 'woopack' ),
						),
					),
					'filter_alignment'  => array(
                        'type'              => 'align',
                        'label'             => __('Alignment', 'woopack'),
                        'default'           => 'left',
                    ),
                    'filter_color' => array(
                        'type'          => 'color',
                        'label'         => __('Text Color', 'woopack'),
                        'default'       => '',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'property'      => 'color',
                            'selector'      => '.woopack-product-filters .woopack-product-filter'
                        )
                    ),
                    'filter_hover_color' 		=> array(
                        'type'          => 'color',
                        'label'         => __('Text Hover Color', 'woopack'),
                        'default'       => '',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'none',
                        )
                    ),
                    'filter_bg_color' 			=> array(
                        'type'          => 'color',
                        'label'         => __('Background Color', 'woopack'),
                        'default'       => '',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'css',
                            'property'      => 'background-color',
                            'selector'      => '.woopack-product-filters .woopack-product-filter'
                        )
                    ),
                    'filter_bg_hover_color' 	=> array(
                        'type'          => 'color',
                        'label'         => __('Background Hover Color', 'woopack'),
                        'default'       => '',
                        'show_reset'    => true,
                        'preview'       => array(
                            'type'          => 'none',
                        )
                    ),
                    'filter_border'     		=> array(
                        'type'              => 'select',
                        'label'             => __('Border', 'woopack'),
                        'default'           => 'solid',
                        'options'           => array(
                            'solid'             => __('Solid', 'woopack'),
                            'double'            => __('Double', 'woopack'),
                            'dotted'            => __('Dotted', 'woopack'),
                        )
                    ),
                    'filter_border_pos'     	=> array(
                        'type'              => 'select',
                        'label'             => __('Border Position', 'woopack'),
                        'default'           => 'solid',
                        'options'           => array(
                            'default'           => __('Default', 'woopack'),
                            'top'               => __('Top', 'woopack'),
                            'bottom'            => __('Bottom', 'woopack'),
                        )
                    ),
                    'filter_border_el'      	=> array(
                        'type'                  => 'select',
                        'label'                 => __('Apply Border on', 'woopack'),
                        'default'               => 'active',
                        'options'               => array(
                            'active'                => __('Active Filter', 'woopack'),
                            'all'                   => __('All Filters', 'woopack')
                        )
                    ),
                    'filter_border_width'   	=> array(
                        'type'                  => 'unit',
                        'label'                 => __('Border Width', 'woopack'),
                        'default'               => '0',
                        'units'					=> array('px'),
                        'slider'				=> true,
                    ),
                    'filter_border_color'   	=> array(
                        'type'                  => 'color',
                        'label'                 => __('Border Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true
                    ),
                    'filter_border_hover_color'	=> array(
                        'type'                      => 'color',
                        'label'                     => __('Border Hover Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true
                    ),
                    'filter_padding_v'    		=> array(
                        'type'              => 'unit',
                        'label'             => __('Padding Top/Bottom', 'woopack'),
                        'default'           => '10',
                        'units'				=> array('px'),
                        'slider'			=> true,
                    ),
                    'filter_padding_h'    		=> array(
                        'type'              => 'unit',
                        'label'             => __('Padding Left/Right', 'woopack'),
                        'default'           => '10',
                        'units'				=> array('px'),
                        'slider'			=> true,
                    ),
                    'filter_margin_h'     		=> array(
                        'type'              => 'unit',
                        'label'             => __('Horizontal Spacing', 'woopack'),
                        'default'           => '20',
                        'units'				=> array('px'),
                        'slider'			=> true,
                        'preview'           => array(
                            'type'              => 'css',
                            'property'          => 'margin-right',
                            'selector'          => '.woopack-product-filters .woopack-product-filter',
                            'unit'              => 'px'
                        )
                    ),
                    'filter_margin_v'     		=> array(
                        'type'              => 'unit',
                        'label'             => __('Vertical Spacing', 'woopack'),
                        'default'           => '20',
                        'units'				=> array('px'),
                        'slider'			=> true,
                        'preview'           => array(
                            'type'              => 'css',
                            'property'          => 'margin-bottom',
                            'selector'          => '.woopack-product-filters .woopack-product-filter',
                            'unit'              => 'px'
                        )
                    ),
                    'filter_radius'     		=> array(
                        'type'              => 'unit',
                        'label'             => __('Round Corners', 'woopack'),
                        'default'           => '0',
                        'units'				=> array('px'),
                        'slider'			=> true,
                        'preview'           => array(
                            'type'              => 'css',
                            'property'          => 'border-radius',
                            'selector'          => '.woopack-product-filters .woopack-product-filter',
                            'unit'              => 'px'
                        )
                    )
                )
            ),
            'filter_typography' => array(
				'title'             => __('Filter Typography', 'bb-powerpack'),
				'collapsed'			=> true,
                'fields'            => array(
					'filter_typography'		=> array(
						'type'        	    	=> 'typography',
						'label'       	    	=> __( 'Typography', 'woopack' ),
						'responsive'  	    	=> true,
						'preview'           	=> array(
							'type'             		=> 'css',
							'selector' 		   		=> '.woopack-product-filters .woopack-product-filter',
						),
					),
                )
            )
        )
    ),
    'pagination'=> array(
        'title'     => __( 'Pagination', 'woopack' ),
        'sections'  => array(
            'pagination'            => array(
                'title'                 => __('Pagination', 'woopack'),
                'fields'                => array(
                    'pagination'            => array(
                        'type'                  => 'select',
                        'label'                 => __('Pagination Style', 'woopack'),
                        'default'               => 'numbers',
                        'options'               => array(
                            'numbers'               => __('Numbers', 'woopack'),
                            'scroll'                => __('Scroll', 'woopack'),
                            'none'                  => _x( 'None', 'Pagination style.', 'woopack' ),
                        ),
                        'toggle'                =>  array(
                            'numbers'               => array(
                                'sections'          => array( 'pagination_font', 'pagination_padding', 'pagination_margin', 'pagination_border', 'pagination_color', 'pagination_hover', 'pagination_property' ),
                                'fields'            => array('pagination_align'),
                            ),
                        ),
                    ),
                    'no_results_message'    => array(
                        'type'                  => 'text',
                        'label'                 => __('No Results Message', 'woopack'),
                        'default'               => __("Sorry, we couldn't find any products. Please try a different search.", 'woopack'),
                        'connections'           => array('string'),
                    ),
                    'show_search'           => array(
                        'type'                  => 'select',
                        'label'                 => __('Show Search', 'woopack'),
                        'default'               => '1',
                        'options'               => array(
                            '1'                     => __('Show', 'woopack'),
                            '0'                     => __('Hide', 'woopack')
                        ),
                        'help'                  => __( 'Shows the search form if no products are found.', 'woopack' )
                    ),
                    'pagination_align'      => array(
                        'type'                  => 'select',
                        'label'                 => __('Alignment', 'woopack'),
                        'default'               => 'center',
                        'options'               => array(
                            'default'               => __('Default', 'woopack'),
                            'left'                  => __('Left', 'woopack'),
                            'center'                => __('Center', 'woopack'),
                            'right'                 => __('Right', 'woopack'),
                        ),
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.fl-builder-pagination ul.page-numbers',
                            'property'              => 'text-align',
                        ),
                    ),
                ),
            ),
            'pagination_color'      => array(
				'title'                 => __('Colors', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'pagination_bg_color'       => array(
                        'type'                      => 'color',
                        'label'                     => __('Background Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.fl-builder-pagination ul.page-numbers li a.page-numbers, .fl-builder-pagination ul.page-numbers li span.page-numbers',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'pagination_bg_color_hover'	=> array(
                        'type'                      => 'color',
                        'label'                     => __('Background Hover Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.fl-builder-pagination ul.page-numbers li .page-numbers:hover, .fl-builder-pagination ul.page-numbers li .page-numbers.current',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'pagination_color'          => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.fl-builder-pagination ul.page-numbers li a.page-numbers, .fl-builder-pagination ul.page-numbers li span.page-numbers',
                            'property'                  => 'color',
                        ),
                    ),
                    'pagination_color_hover'    => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Hover Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.fl-builder-pagination ul.page-numbers li a.page-numbers:hover, .fl-builder-pagination ul.page-numbers li .page-numbers.current',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'pagination_border'     => array(
				'title'                 => __('Border', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
					'pagination_border'		=> array(
                        'type'					=> 'border',
                        'label'					=> __('Border Style', 'woopack'),
                        'responsive'			=> true,
                        'preview'				=> array(
                            'type'					=> 'css',
                            'property'				=> 'border-color',
                            'selector'				=> '.fl-builder-pagination ul.page-numbers li .page-numbers',
                        ),
                    ),
                    'pagination_border_color_hover' => array(
                        'type'                          => 'color',
                        'label'                         => __('Border Hover Color', 'woopack'),
                        'show_reset'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.fl-builder-pagination ul.page-numbers li .page-numbers:hover, .fl-builder-pagination ul.page-numbers li .page-numbers.current',
                            'property'                      => 'border-color',
                        ),
                    ),
                ),
            ),
            'pagination_padding'    => array(
				'title'                 => __('Padding', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'pagination_padding'    => array(
                        'type'					=> 'dimension',
                        'label'					=> __('Padding', 'woopack'),
						'slider'		        => true,
						'units'		  	        => array( 'px' ),
                        'preview'				=> array(
                            'type'					=> 'css',
                            'selector'				=> '.fl-builder-pagination ul.page-numbers li .page-numbers',
                            'property'				=> 'padding',
                            'unit'					=> 'px'
                        ),
                        'responsive'			=> true,
                    ),
                ),
            ),
            'pagination_margin'     => array(
				'title'                 => __('Margin', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'pagination_margin'     => array(
                        'type'					=> 'dimension',
                        'label'					=> __('Margin', 'woopack'),
						'slider'				=> true,
						'units'					=> array( 'px' ),
                        'preview'				=> array(
                            'type'					=> 'css',
                            'selector'				=> '.fl-builder-pagination ul.page-numbers li .page-numbers',
                            'property'				=> 'margin',
                            'unit'					=> 'px'
                        ),
                        'responsive'			=> true,
                    ),
                ),
            ),
            'pagination_font'       => array(
				'title'                 => __('Typography', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
					'pagination_typography'		=> array(
						'type'        	    	=> 'typography',
						'label'       	    	=> __( 'Typography', 'woopack' ),
						'responsive'  	    	=> true,
						'preview'           	=> array(
							'type'             		=> 'css',
							'selector' 		   		=> '.fl-builder-pagination ul.page-numbers li .page-numbers',
						),
					),
                ),
            ),
        ),
    ),
    'style'     => array(
        'title'     => __('Style', 'woopack'),
        'sections'  => apply_filters( 'woopack_product_grid_style_sections', woopack_product_style_fields() )
    ),
    'button'    => array(
        'title'     => __('Button', 'woopack'),
        'sections'  => woopack_product_button_fields()
    ),
    'typography'=> array(
        'title'     => __('Typography', 'woopack'),
        'sections'  => woopack_product_typography_fields()
    ),
));
