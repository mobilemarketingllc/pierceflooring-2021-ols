;(function($) {

	FLBuilder.registerModuleHelper('product-grid', {
		_templates: '',

		init: function() {
			this._setTemplates();

			var form = $('.fl-builder-settings'),
				self = this;
			form.find( 'select[name="layout"]' ).on('change', function() {
				self._addCssClass();
			});
			this._addCssClass();
		},

		_getTemplates: function(callback) {
			if ( 'undefined' === typeof callback ) {
				return;
			}

			$.post(
				ajaxurl,
				{
					action: 'woopack_get_fl_theme_layouts',
				},
				function( response ) {
					callback(response);
				}
			);
		},

		_setTemplates: function() {
			var form = $('.fl-builder-settings'),
				select = form.find( 'select[name="quick_view_template"]' ),
				value = '',
				self = this;

			if ( select.length === 0 ) {
				return;
			}

			if ( 'undefined' !== typeof FLBuilderSettingsForms && 'undefined' !== typeof FLBuilderSettingsForms.config ) {
				if ( "product-grid" === FLBuilderSettingsForms.config.id ) {
					value = FLBuilderSettingsForms.config.settings['quick_view_template'];
				}
			}

			if ( this._templates !== '' ) {
				select.html( this._templates );
				select.find( 'option[value="' + value + '"]').attr('selected', 'selected');

				return;
			}

			this._getTemplates(function(response) {
				if ( response.success ) {
					self._templates[type] = response.data;
					select.html( response.data );
					if ( '' !== value ) {
						select.find( 'option[value="' + value + '"]').attr('selected', 'selected');
					}
				}
			});
		},

		_addCssClass: function() {
			var form = $('.fl-builder-settings'),
				layout = form.find( 'select[name="layout"]' ).val();

			form.removeClass( 'woopack-layout--pre-defined' );
			form.removeClass( 'woopack-layout--custom' );

			form.addClass( 'woopack-layout--' + layout );
		}
	});

})(jQuery);