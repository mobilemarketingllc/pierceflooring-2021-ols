<?php
/**
* @class WooPackOffcanvasCart
*/
class WooPackOffcanvasCart extends FLBuilderModule {
	/**
	* Constructor function for the module. You must pass the
	* name, description, dir and url in an array to the parent class.
	*
	* @method __construct
	*/
	public function __construct() {
		parent::__construct( array(
			'name' 			=> __( 'Off-Canvas Cart', 'woopack' ),
			'description' 	=> __( 'Module to display Off-Canvas Cart.', 'woopack' ),
			'group'         => WooPack_Helper::get_modules_group(),
			'category' 		=> WOOPACK_CAT,
			'dir' 			=> WOOPACK_DIR . 'modules/offcanvas-cart/',
			'url' 			=> WOOPACK_URL . 'modules/offcanvas-cart/',
			'editor_export' => true, // Defaults to true and can be omitted.
			'enabled' 		=> true, // Defaults to true and can be omitted.
		));

		add_filter( 'woocommerce_add_to_cart_fragments', array( $this, 'woopack_cart_fragment' ) );
	}

	/**
	 * Woopack Mini Cart Counter Fragment
	 *
	 * @since 1.0.0
	 * @return void
	 */

	public function woopack_cart_fragment( $fragments ) {
		ob_start();
		?>
		<span class="cart-counter"><?php echo is_object( WC()->cart ) ? WC()->cart->get_cart_contents_count() : '0'; ?></span>
		<?php
		
		$fragments['span.cart-counter'] = ob_get_clean();

		remove_filter( 'woocommerce_add_to_cart_fragments', array( $this, 'woopack_cart_fragment' ) );
		
		return $fragments;
	}
}

/**
* Register the module and its form settings.
*/
FLBuilder::register_module( 'WooPackOffcanvasCart', array(
	'general'		=> array(
		'title'			=> __('General', 'woopack'),
		'sections'		=> array(
			'general'		=> array(
				'title'			=> '',
				'fields'		=> array(
					'show_preview'	=> array(
						'type'			=> 'button',
						'label'			=> __('Preview', 'woopack'),
						'class'			=> 'woopack-offcanvas-cart-preview'
					)
				)
			),
			'button'   => array(
				'title'  	=> __( 'Button', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'counter_position'	=> array(
						'type'    => 'select',
						'label'   => __( 'Counter Position', 'woopack' ),
						'default' => 'after',
						'options' => array(
							'before' => __( 'Before Button', 'woopack' ),
							'after'  => __( 'After Button', 'woopack' ),
							'top'    => __( 'Bubble', 'woopack' ),
						),
						'toggle'  => array(
							'before' => array(
								'fields' => array( 'cart_counter_padding' ),
							),
							'top' => array(
								'fields' => array( 'cart_counter_width' ),
							),
							'after' => array(
								'fields' => array( 'cart_counter_padding' ),
							),
						),
					),
					'icon_style' 		=> array(
						'type'    => 'select',
						'label'   => __( 'Style', 'woopack' ),
						'default' => 'text',
						'options' => array(
							'icon'      => __( 'Icon only', 'woopack' ),
							'icon_text' => __( 'Icon + Text', 'woopack' ),
							'text'      => __( 'Text only', 'woopack' ),
						),
						'toggle'  => array(
							'icon' => array(
								'sections' => array( 'icon_type', 'icon_button_style_icon' ),
							),
							'icon_text' => array(
								'fields'   => array( 'cart_text', 'icon_button_spacing' ),
								'sections' => array( 'icon_type', 'icon_button_style_icon', 'cart_text_font' ),
							),
							'text' => array(
								'fields'   => array( 'cart_text' ),
								'sections' => array( 'cart_text_font' ),
							),
						),
					),
					'cart_text'  		=> array(
						'type'    => 'text',
						'label'   => __( 'Text', 'woopack' ),
						'default' => __('Cart', 'woopack')
					),
				),
			),
			'icon_type' => array(
				'title'  	=> __( 'Icon', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'icon_type'  => array(
						'type'      => 'select',
						'label'     => __( 'Icon Type', 'woopack' ),
						'default'   => 'icon',
						'options'   => array(
							'icon'  => __( 'Icon', 'woopack' ),
							'image' => __( 'Image', 'woopack' ),
						),
						'toggle'        => array(
							'icon' => array(
								'fields' => array( 'cart_icon', 'cart_icon_color', 'cart_icon_hover_color' ),
							),
							'image' => array(
								'fields' => array( 'cart_image' ),
							),
						),
					),
					'cart_icon'  => array(
						'type'  => 'icon',
						'label' => __( 'Choose Icon', 'woopack' ),
					),
					'cart_image' => array(
						'type'      => 'photo',
						'label'     => __( 'Image Icon', 'woopack' ),
						'show_remove'   => true,
						'connections'   => array( 'photo' ),
					),
				),
			),
		)
	),
	'style'			=> array(
		'title'			=> __('Style', 'woopack'),
		'sections'		=> array(
			'icon_button_structure'	=> array(
				'title'  	=> __( 'Cart Button Structure', 'woopack' ),
				'collapsed'	=> false,
				'fields' 	=> array(
					'cart_button_alignment'      => array(
						'type'		=> 'align',
						'label'		=> __( 'Alignment', 'woopack' ),
						'default'	=> 'left',
						'responsive'=> true,
					),
					'icon_button_padding'       => array(
						'type'			=> 'dimension',
						'label'			=> __( 'Padding', 'woopack' ),
						'default'		=> '10',
						'units'			=> array('px'),
						'slider'		=> true,
						'responsive'	=> true,
						'preview'		=> array(
							'type'			=> 'css',
							'selector'		=> '.woopack-cart-contents',
							'property'		=> 'padding',
							'unit'			=> 'px',
						),
					),
					'icon_button_spacing'       => array(
						'type'			=> 'unit',
						'label'			=> __( 'Space Between Icon & Text', 'woopack' ),
						'default'		=> '5',
						'units'			=> array('px'),
						'slider'		=> true,
						'responsive'	=> true,
						'preview'		=> array(
							'type'			=> 'css',
							'selector'		=> '.woopack-cart-icon_text .cart-contents-icon',
							'property'		=> 'padding-right',
							'unit'			=> 'px',
						),
					),
                    'cart_button_border_group'	=> array(
                        'type'                  => 'border',
                        'label'                 => __('Border Style', 'woopack'),
                        'responsive'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector' 		        => '.woopack-cart-contents',
                        ),
					),
					'cart_button_border_color_hr' => array(
						'type'       => 'color',
						'label'      => __( 'Border Hover Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'show_alpha' => true,
						'preview'		=> array(
							'type'			=> 'none'
						)
					),
				),
			),
			'icon_button_style'     => array(
				'title'  	=> __( 'Cart Button Colors', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'cart_button_bg_color'       => array(
						'type'       => 'color',
						'label'      => __( 'Background Color', 'woopack' ),
						'default'    => 'ffffff',
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-contents',
							'property' => 'background-color',
						),
					),
					'cart_button_bg_hover_color' => array(
						'type'       => 'color',
						'label'      => __( 'Background Hover Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'show_alpha' => true,
						'preview'		=> array(
							'type'			=> 'none'
						)
					),
					'cart_text_color'		=> array(
						'type'       => 'color',
						'label'      => __( 'Text Color', 'woopack' ),
						'default'    => '000000',
						'show_reset' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.cart-contents-text',
							'property' => 'color',
						),
					),
					'cart_text_hover_color'	=> array(
						'type'       => 'color',
						'label'      => __( 'Text Hover Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-contents:hover .cart-contents-text',
							'property' => 'color',
						),
					),
				),
			),
			'icon_button_style_icon' => array(
				'title'  	=> __( 'Cart Button Icon', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'cart_icon_font_size'        => array(
						'type'			=> 'unit',
						'label'			=> __( 'Size', 'woopack' ),
						'default'		=> '20',
						'units'			=> array('px'),
						'slider'		=> true,
						'responsive'	=> true,
						'preview'		=> array(
							'type'  => 'css',
							'rules' => array(
								array(
									'selector' => '.cart-contents-icon',
									'property' => 'font-size',
									'unit'     => 'px',
								),
								array(
									'selector' => '.cart-contents-image',
									'property' => 'width',
									'unit'     => 'px',
								),
								array(
									'selector' => '.cart-contents-image',
									'property' => 'height',
									'unit'     => 'px',
								),
							),
						),
					),
					'cart_icon_color'            => array(
						'type'       => 'color',
						'label'      => __( 'Icon Color', 'woopack' ),
						'default'    => '000000',
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.cart-contents-icon',
							'property' => 'color',
						),
					),
					'cart_icon_hover_color'      => array(
						'type'       => 'color',
						'label'      => __( 'Icon Hover Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-contents:hover .cart-contents-icon',
							'property' => 'color',
						),
					),
				),
			),
			'counter_style'         => array(
				'title'  	=> __( 'Counter', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'cart_counter_bg_color'      => array(
						'type'       => 'color',
						'label'      => __( 'Background Color', 'woopack' ),
						'default'    => '757575',
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-contents .cart-counter',
							'property' => 'background-color',
						),
					),
					'cart_counter_bg_hover_color'      => array(
						'type'       => 'color',
						'label'      => __( 'Background Hover Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'none',
						),
					),
					'cart_counter_color'		=> array(
						'type'       => 'color',
						'label'      => __( 'Text Color', 'woopack' ),
						'default'    => 'ffffff',
						'show_reset' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-contents .cart-counter',
							'property' => 'color',
						),
					),
					'cart_counter_hover_color'		=> array(
						'type'       => 'color',
						'label'      => __( 'Text Hover Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'preview'    => array(
							'type'     => 'none',
						),
					),
					'cart_counter_width'         => array(
						'type'        	=> 'unit',
						'label'       	=> __( 'Size', 'woopack' ),
						'default'     	=> '20',
						'units'			=> array('px'),
						'slider'		=> true,
						'responsive'  	=> true,
						'preview'		=> array(
							'type'			=> 'css',
							'rules'			=> array(
								array(
									'property'		=> 'width',
									'selector'		=> '.cart-contents-count span',
									'unit'			=> 'px'
								),
								array(
									'property'		=> 'height',
									'selector'		=> '.cart-contents-count span',
									'unit'			=> 'px'
								),
								array(
									'property'		=> 'line-height',
									'selector'		=> '.cart-contents-count span',
									'unit'			=> 'px'
								),
							)
						)
					),
					'cart_counter_padding'       => array(
						'type'        	=> 'dimension',
						'label'       	=> __( 'Padding', 'woopack' ),
						'default'     	=> '10',
						'units'			=> array('px'),
						'slider'		=> true,
						'responsive'	=> true,
						'preview'     	=> array(
							'type'     		=> 'css',
							'selector' 		=> '.cart-contents-count-before span, .cart-contents-count-after span',
							'property' 		=> 'padding',
							'unit'     		=> 'px',
						),
					),
                    'cart_counter_border_group'	=> array(
                        'type'                  => 'border',
                        'label'                 => __('Border Style', 'woopack'),
                        'responsive'            => true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector' 		        => '.woopack-cart-contents .cart-counter',
                        ),
                    ),
				),
			),
		)
	),
	'offcanvas'            => array(
		'title'    => __( 'Panel', 'woopack' ),
		'sections' => array(
			'panel_general'	=> array(
				'title'			=> __('General', 'bb-powerpack'),
				'fields'		=> array(
					'direction'		=> array(
						'type'			=> 'select',
						'label'			=> __('Direction', 'woopack'),
						'default'		=> 'right',
						'options'		=> array(
							'left'			=> __('Left', 'woopack'),
							'right'			=> __('Right', 'woopack'),
						),
						'preview'		=> array(
							'type'			=> 'none'
						)
					),
					'width'		=> array(
						'type'			=> 'unit',
						'label'			=> __( 'Width', 'woopack' ),
						'default'		=> '',
						'units'			=> array('px', '%'),
						'slider'		=> true,
						'responsive'	=> true,
					),
				)
			),
			'box_style'             => array(
				'title'  	=> __( 'Items Container', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'box_bg_color'   => array(
						'type'       => 'color',
						'label'      => __( 'Background Color', 'woopack' ),
						'default'    => 'ffffff',
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-items',
							'property' => 'background-color',
						),
					),
				),
			),
			'cart_item_style'       => array(
				'title'  	=> __( 'Item', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'cart_item_odd_color'    => array(
						'type'       => 'color',
						'label'      => __( 'Odd Item Background Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'default'    => '',
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-items ul li.woocommerce-mini-cart-item:nth-child(odd)',
							'property' => 'background',
						),
					),
					'cart_item_even_color'   => array(
						'type' 				     => 'color',
						'label' 			     => __( 'Even Item Background Color', 'woopack' ),
						'show_reset' 		     => true,
						'show_alpha' 		     => true,
						'default'                => '',
						'preview' 			     => array(
							'type' 				     => 'css',
							'selector'			     => '.woopack-cart-items ul li.woocommerce-mini-cart-item:nth-child(even)',
							'property'      	     => 'background',
						),
					),
					'cart_item_padding'    => array(
						'type'        	=> 'dimension',
						'label'       	=> __( 'Padding', 'woopack' ),
						'default'     	=> '10',
						'units'			=> array('px'),
						'slider'		=> true,
						'responsive'	=> true,
					),
					'cart_item_border_width' => array(
						'type'        	=> 'unit',
						'label'			=> __( 'Separator Width', 'woopack' ),
						'default'		=> '1',
						'units'			=> array('px'),
						'slider'		=> true,
						'responsive'  	=> true,
						'preview'     	=> array(
							'type'     		=> 'css',
							'selector' 		=> '.woopack-cart-items ul li.woocommerce-mini-cart-item',
							'property' 		=> 'border-bottom-width',
							'unit'     		=> 'px',
						),
					),
					'cart_item_border_color' => array(
						'type'       => 'color',
						'label'      => __( 'Separator Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'default'    => 'efefef',
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-items ul li.woocommerce-mini-cart-item',
							'property' => 'border-color',
						),
					),
				),
			),
			'product_name_style'    => array(
				'title'  	=> __( 'Item Name', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'product_name_color'            => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-items ul li.woocommerce-mini-cart-item a:not(.remove_from_cart_button)',
							'property' => 'color',
						),
					),
					'product_margin' => array(
						'type'        	=> 'unit',
						'label'       	=> __( 'Margin Bottom', 'woopack' ),
						'units'			=> array('px'),
						'slider'		=> true,
						'responsive'  	=> true,
						'preview'     	=> array(
							'type'     		=> 'css',
							'selector' 		=> '.woopack-cart-items ul li.woocommerce-mini-cart-item a:not(.remove_from_cart_button)',
							'property' 		=> 'margin-bottom',
							'unit'     		=> 'px',
						),
					),
				),
			),
			'qty_price'		=> array(
				'title'			=> __('Item Quantity & Price', 'woopack'),
				'collapsed'		=> true,
				'fields'		=> array(
					'quantity_color'            => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-items ul li.woocommerce-mini-cart-item .quantity',
							'property' => 'color',
						),
					),
				)
			),
			'image_style'           => array(
				'title'  	=> __( 'Item Image', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'show_image'  => array(
						'type'    => 'select',
						'label'   => __( 'Show Image?', 'woopack' ),
						'default' => 'yes',
						'options' => array(
							'yes' => __( 'Yes', 'woopack' ),
							'no'  => __( 'No', 'woopack' ),
						),
						'toggle'  => array(
							'yes' => array(
								'fields' => array( 'image_width', 'image_position' ),
							),
						),
					),
					'image_width' => array(
						'type'        	=> 'unit',
						'label'       	=> __( 'Size', 'woopack' ),
						'units'			=> array('px'),
						'slider'		=> true,
						'responsive'  	=> true,
						'preview'     	=> array(
							'type'     		=> 'css',
							'selector' 		=> '.woopack-cart-items ul li.woocommerce-mini-cart-item a img',
							'property' 		=> 'width',
							'unit'     		=> 'px',
						),
					),
					'image_position'	=> array(
						'type'				=> 'select',
						'label'				=> __('Position', 'woopack'),
						'default'			=> 'left',
						'options'			=> array(
							'left'				=> __('Left', 'woopack'),
							'right'				=> __('Right', 'woopack')
						)
					)
				),
			),
			'product_remove_style'  => array(
				'title'  	=> __( 'Remove Item Icon', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'product_remove_font_size'        => array(
						'type'    => 'select',
						'label'   => __( 'Size', 'woopack' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'Default', 'woopack' ),
							'custom'  => __( 'Custom', 'woopack' ),
						),
						'toggle'  => array(
							'custom' => array(
								'fields' => array( 'product_remove_font_size_custom' ),
							),
						),
					),
					'product_remove_font_size_custom' => array(
						'type'        	=> 'unit',
						'label'       	=> __( 'Custom Size', 'woopack' ),
						'units'			=> array('px'),
						'slider'		=> true,
						'responsive'  	=> true,
						'preview'     	=> array(
							'type'     		=> 'css',
							'selector' 		=> '.woopack-cart-items ul li.woocommerce-mini-cart-item a.remove.remove_from_cart_button',
							'property' 		=> 'font-size',
							'unit'     		=> 'px',
						),
					),
					'product_remove_position'	=> array(
						'type'				=> 'select',
						'label'				=> __('Position', 'woopack'),
						'default'			=> 'right',
						'options'			=> array(
							'left'				=> __('Left', 'woopack'),
							'right'				=> __('Right', 'woopack')
						)
					),
					'product_remove_color'            => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-items ul li.woocommerce-mini-cart-item a.remove.remove_from_cart_button',
							'property' => 'color',
						),
					),
					'product_remove_color_hover'      => array(
						'type'       => 'color',
						'label'      => __( 'Hover Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
					),
					'product_remove_bg_color'            => array(
						'type'       => 'color',
						'label'      => __( 'Background Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-items ul li.woocommerce-mini-cart-item a.remove.remove_from_cart_button',
							'property' => 'background-color',
						),
					),
					'product_remove_bg_color_hover'      => array(
						'type'       => 'color',
						'label'      => __( 'Background Hover Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'show_alpha' => true,
					),
				),
			),
			'subtotal_style'        => array(
				'title'  	=> __( 'Subtotal', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'subtotal_color'            => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-items .woocommerce-mini-cart__total.total',
							'property' => 'color',
						),
					),
					'subtotal_text_align' => array(
						'type'		=> 'align',
						'label'   	=> __( 'Alignment', 'woopack' ),
						'default' 	=> 'right',
						'preview' 	=> array(
							'type'     	=> 'css',
							'selector' 	=> '.woopack-cart-items .woocommerce-mini-cart__total.total',
							'property' 	=> 'text-align',
						),
					),
				),
			),
			'button_structure'      => array(
				'title'  => __( 'Button Structure', 'woopack' ),
				'collapsed'	=> true,
				'fields' => array(
					'button_alignment'     => array(
						'type'	  => 'align',
						'label'	  => __( 'Alignment', 'woopack' ),
						'default' => 'right',
						'preview' => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-items .woocommerce-mini-cart__buttons',
							'property' => 'text-align',
						),
					),
					'button_width'         => array(
						'type'  	=> 'select',
						'label' 	=> __( 'Width', 'woopack' ),
						'default' 	=> 'auto',
						'options' 	=> array(
							'auto'  	=> __( 'Auto', 'woopack' ),
							'full_width' => __( 'Full Width', 'woopack' ),
							'custom' 	=> __( 'Custom', 'woopack' ),
						),
						'toggle' => array(
							'full_width'	=> array(
								'fields'		=> array('button_spacing')
							),
							'custom' => array(
								'fields' => array( 'button_width_custom', 'button_spacing' ),
							),
						),
					),
					'button_width_custom'  => array(
						'type'              	=> 'unit',
						'label'                 => __( 'Custom Width', 'woopack' ),
						'units'					=> array('%'),
						'slider'				=> true,
						'responsive' 			=> true,
					),
					'button_spacing'       => array(
						'type'        			=> 'unit',
						'label'       			=> __( 'Space Between', 'woopack' ),
						'default'     			=> 10,
						'units'					=> array('px'),
						'slider'				=> true,
						'responsive' 			=> true,
					),
					'button_padding'       => array(
						'type'        			=> 'dimension',
						'label'       			=> __( 'Padding', 'woopack' ),
						'units'					=> array('px'),
						'slider'				=> true,
						'responsive' 			=> true,
						'preview'     			=> array(
							'type'     				=> 'css',
							'selector' 				=> '.woopack-cart-items .woocommerce-mini-cart__buttons .button',
							'property' 				=> 'padding',
							'unit'     				=> 'px',
						),
					),
				),
			),
			'button_color_view'     => array(
				'title'  	=> __( 'View Cart Button', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'view_button_bg_color'			=> array(
						'type'       => 'color',
						'label'      => __( 'Background Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type' => 'css',
							'selector' => '.woopack-cart-items .woocommerce-mini-cart__buttons .button',
							'property' => 'background-color',
						),
					),
					'view_button_bg_color_hover'	=> array(
						'type'       => 'color',
						'label'      => __( 'Background Hover Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
					),
					'view_button_color'				=> array(
						'type'       => 'color',
						'label'      => __( 'Text Color', 'woopack' ),
						'show_reset' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-items .woocommerce-mini-cart__buttons .button',
							'property' => 'color',
						),
					),
					'view_button_color_hover'		=> array(
						'type'       => 'color',
						'label'      => __( 'Text Hover Color', 'woopack' ),
						'show_reset' => true,
					),
                    'view_button_border_group'		=> array(
                        'type'				=> 'border',
                        'label'				=> __('Border Style', 'woopack'),
                        'responsive'		=> true,
                        'preview'				=> array(
                            'type'				=> 'css',
                            'selector'			=> '.woopack-cart-items .woocommerce-mini-cart__buttons .button',
                        ),
                    ),
					'view_button_border_color_hover'=> array(
						'type'       => 'color',
						'label'      => __( 'Border Hover Color', 'woopack' ),
						'show_reset' => true,
					),
				),
			),
			'button_color_checkout' => array(
				'title'  	=> __( 'Checkout Button', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'checkout_button_bg_color'           => array(
						'type'       => 'color',
						'label'      => __( 'Background Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type' => 'css',
							'selector' => '.woopack-cart-items .woocommerce-mini-cart__buttons .button.checkout',
							'property' => 'background-color',
						),
					),
					'checkout_button_bg_color_hover'     => array(
						'type'       => 'color',
						'label'      => __( 'Background Hover Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
					),
					'checkout_button_color'              => array(
						'type'       => 'color',
						'label'      => __( 'Text Color', 'woopack' ),
						'show_reset' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-cart-items .woocommerce-mini-cart__buttons .button.checkout',
							'property' => 'color',
						),
					),
					'checkout_button_color_hover'        => array(
						'type'       => 'color',
						'label'      => __( 'Text Hover Color', 'woopack' ),
						'show_reset' => true,
					),
                    'checkout_button_border_group'		=> array(
                        'type'				=> 'border',
                        'label'				=> __('Border Style', 'woopack'),
                        'responsive'		=> true,
                        'preview'				=> array(
                            'type'				=> 'css',
                            'selector'			=> '.woopack-cart-items .woocommerce-mini-cart__buttons .button.checkout',
                        ),
                    ),
					'checkout_button_border_color_hover' => array(
						'type'       => 'color',
						'label'      => __( 'Border Hover Color', 'woopack' ),
						'show_reset' => true,
					),
				),
			),
			'close_style'  => array(
				'title'  		=> __( 'Offcanvas Close Icon', 'woopack' ),
				'collapsed'		=> true,
				'fields' 		=> array(
					'close_font_size'        => array(
						'type'    => 'select',
						'label'   => __( 'Size', 'woopack' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'Default', 'woopack' ),
							'custom'  => __( 'Custom', 'woopack' ),
						),
						'toggle'  => array(
							'custom' => array(
								'fields' => array( 'close_font_size_custom' ),
							),
						),
					),
					'close_font_size_custom' => array(
						'type'        	=> 'unit',
						'label'       	=> __( 'Custom Size', 'woopack' ),
						'units'			=> array('px'),
						'slider'		=> true,
						'responsive'  	=> true,
					),
					'close_color'            => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
					),
					'close_color_hover'      => array(
						'type'       => 'color',
						'label'      => __( 'Hover Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
					),
				),
			),
		),
	),
	'typography'        => array(
		'title'    => __( 'Typography', 'woopack' ),
		'sections' => array(
			'cart_text_font'    => array(
				'title'  => __( 'Cart Button Text', 'woopack' ),
				'fields' => array(
                    'cart_text_typography'	=> array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.cart-contents-text',
						),
					),
				),
			),
			'cart_counter_font' => array(
				'title'  	=> __( 'Cart Counter', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
                    'cart_counter_typography'	=> array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.cart-counter',
						),
					),
				),
			),
			'product_name_font' => array(
				'title'  	=> __( 'Item Name', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
                    'product_name_typography'	=> array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.woopack-cart-items ul li.woocommerce-mini-cart-item a:not(.remove_from_cart_button)',
						),
					),
				),
			),
			'quantity_font'     => array(
				'title'  	=> __( 'Quantity & Price', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
                    'quantity_typography'	=> array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.woopack-cart-items ul li.woocommerce-mini-cart-item .quantity',
						),
					),
				),
			),
			'subtotal_font'     => array(
				'title'  	=> __( 'Subtotal', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
                    'subtotal_typography'	=> array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.woopack-cart-items .woocommerce-mini-cart__total.total',
						),
					),
				),
			),
			'button_font'      => array(
				'title'  	=> __( 'View Cart / Checkout Buttons', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
                    'button_typography'	=> array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Typography', 'woopack' ),
                        'responsive'  	    => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector' 		    => '.woopack-cart-items .woocommerce-mini-cart__buttons .button',
						),
					),
				),
			),
		),
	),
) );