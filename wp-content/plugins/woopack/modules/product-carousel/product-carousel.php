<?php
/**
* @class WooPackProductCarousel
*/
class WooPackProductCarousel extends FLBuilderModule {
    /**
    * Constructor function for the module. You must pass the
    * name, description, dir and url in an array to the parent class.
    *
    * @method __construct
    */
    public function __construct() {
        parent::__construct(array(
            'name' 				=> __('Product Carousel', 'woopack'),
            'description' 		=> __('Addon to display Product Carousel.', 'woopack'),
            'group'             => WooPack_Helper::get_modules_group(),
            'category' 			=> WOOPACK_CAT,
            'dir' 				=> WOOPACK_DIR . 'modules/product-carousel/',
            'url' 				=> WOOPACK_URL . 'modules/product-carousel/',
            'editor_export' 	=> true, // Defaults to true and can be omitted.
            'enabled' 			=> true, // Defaults to true and can be omitted.
		));
		
		add_filter( 'fl_builder_loop_query_args', array( $this, 'exclude_current_post' ), 10, 1 );
    }

    /**
    * @method enqueue_scripts
    */
    public function enqueue_scripts() {
		$this->add_css( 'font-awesome-5' );
        $this->add_js( 'imagesloaded' );
        $this->add_css( 'owl-style', WOOPACK_URL . 'assets/css/owl.carousel.css' );
	    $this->add_css( 'owl-theme', WOOPACK_URL . 'assets/css/owl.theme.css' );
		$this->add_js( 'owl-jquery', WOOPACK_URL . 'assets/js/owl.carousel.min.js', array('jquery'), '', true );

		$this->add_css( 'woopack-modal-box' );
        $this->add_js( 'woopack-modal-box' );
    }

    public function get_layout() {
        //$layout = $this->layout;
        return 1;
	}
	
	public function get_layout_style() {
		return 'carousel';
	}

	public function get_item_html_tag() {
		return 'div';
	}

	public function update( $settings ) {
		$settings->item_html_tag = $this->get_item_html_tag();
		$settings->layout_style = $this->get_layout_style();
		return $settings;
	}

    /**
    * Renders the CSS class for each post item.
    *
    * @since 1.0.0
    * @return void
    */
    public function render_post_class( $product )
    {
        $settings   = $this->settings;
        $layout     = 'carousel';
        $show_image = has_post_thumbnail() && $settings->show_image;
        $classes    = array( 'woopack-product-carousel' );
        $classes[] = 'woopack-product-align-' . $settings->product_align;

        if ( $product ) {
            $classes[] = 'product';
            $classes[] = wc_get_loop_class();
            $classes[] = $product->get_stock_status();

            if ( $product->is_on_sale() ) {
                $classes[] = 'sale';
            }
            if ( $product->is_featured() ) {
                $classes[] = 'featured';
            }
            if ( $product->is_downloadable() ) {
                $classes[] = 'downloadable';
            }
            if ( $product->is_virtual() ) {
                $classes[] = 'virtual';
            }
            if ( $product->is_sold_individually() ) {
                $classes[] = 'sold-individually';
            }
            if ( $product->is_taxable() ) {
                $classes[] = 'taxable';
            }
            if ( $product->is_shipping_taxable() ) {
                $classes[] = 'shipping-taxable';
            }
            if ( $product->is_purchasable() ) {
                $classes[] = 'purchasable';
            }
            if ( $product->get_type() ) {
                $classes[] = "product-type-" . $product->get_type();
            }
            if ( $product->is_type( 'variable' ) ) {
                if ( ! $product->get_default_attributes() ) {
                    $classes[] = 'has-default-attributes';
                }
                if ( $product->has_child() ) {
                    $classes[] = 'has-children';
                }
            }
        }

        if ( false !== ( $key = array_search( 'hentry', $classes ) ) ) {
            unset( $classes[ $key ] );
        }

        post_class( apply_filters( 'woopack_product_carousel_classes', $classes, $settings ) );
	}

	public function exclude_current_post( $args ) {
		return WooPack_Helper::exclude_post( $args );
	}

	public function filter_settings( $settings, $helper ) {

		// Handle old Button Settings.
		$settings = filter_product_button_settings( $settings );

		// Handle old Style Settings.
		$settings = filter_product_style_settings( $settings );

		// Handle old Typography Settings.
		$settings = filter_product_typography_settings( $settings );

        // Return the filtered settings.
		return $settings;
	}
}

/**
* Register the module and its form settings.
*/
FLBuilder::register_module('WooPackProductCarousel', array(
    'general'   => array(
        'title'     => __('General', 'woopack'),
        'sections'  => array(
            'layout'    => array(
                'title'     => __('Layout', 'woopack'),
                'fields'    => array(
                    'product_layout'    => array(
                        'type'              => 'layout',
                        'label'             => __('', 'woopack'),
                        'default'           => 1,
                        'options'           => array(
                            1                    => WOOPACK_URL . 'modules/product-carousel/images/layout-1.png',
                            2                    => WOOPACK_URL . 'modules/product-carousel/images/layout-2.png',
                            3                    => WOOPACK_URL . 'modules/product-carousel/images/layout-3.png',
                            4                    => WOOPACK_URL . 'modules/product-carousel/images/layout-4.png',
                        ),
                    ),
                ),
            ),
            'config'    => array(
				'title'     => __('Structure', 'woopack'),
				'collapsed' => true,
                'fields'    => array(
                    'posts_per_page'    => array(
                        'type'              => 'unit',
                        'label'             => __('Number of Posts', 'woopack'),
                        'default'           => '10',
                        'slider'			=> true,
                    ),
                    'product_columns'   => array(
                        'type'              => 'unit',
						'label'             => __('Columns', 'woopack'),
						'slider'			=> true,
                        'responsive'        => array(
                            'placeholder'       => array(
                                'default' 		    => '3',
                                'medium' 			=> '2',
                                'responsive'		=> '1',
                            ),
                        ),
                    ),
                    'product_spacing'   => array(
                        'type'              => 'unit',
                        'label'             => __('Spacing', 'woopack'),
                        'default'           => '2',
                        'units'				=> array('%'),
                        'slider'			=> true,
                    ),
                    'match_height'      => array(
                        'type'              => 'select',
                        'label'             => __('Equal Heights', 'woopack'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'                => __('No', 'woopack'),
                        ),
                    ),
                    'product_align'     => array(
                        'type'              => 'select',
                        'label'             => __('Alignment', 'woopack'),
                        'default'           => 'default',
                        'options'           => array(
                            'default'           => __('Default', 'woopack'),
                            'left'              => __('Left', 'woopack'),
                            'center'            => __('Center', 'woopack'),
                            'right'             => __('Right', 'woopack')
                        ),
					),	
                ),
            ),
            'slider'    => array(
				'title'         => __('Slider', 'woopack'),
				'collapsed' => true,
                'fields'        => array(
                    'auto_play'             => array(
                        'type'                  => 'select',
                        'label'                 => __('Autoplay', 'woopack'),
                        'default'               => 'yes',
                        'options'               => array(
                            'yes'                   => __('Yes', 'woopack'),
                            'no'                    => __('No', 'woopack'),
                        ),
                        'toggle'                => array(
                            'yes'			        => array(
                                'fields'            => array( 'slider_duration', 'stop_on_hover', 'scroll_as' )
                            ),
                        ),
                    ),
                    'slider_duration'       => array(
                        'type'                  => 'unit',
                        'label'                 => __('Autoplay Timeout', 'woopack'),
                        'default'               => '5',
                        'units'					=> array( 'seconds' ),
                        'slider'				=> true,
                    ),
                    'scroll_as'             => array(
                        'type'                  => 'select',
                        'label'                 => __('Scroll As', 'woopack'),
                        'default'               => 'scrollPerProduct',
                        'options'               => array(
                            'scrollPerProduct'      => __('Per Product', 'woopack'),
                            'scrollPerPage'         => __('Per Page', 'woopack'),
                        ),
                    ),
                    'stop_on_hover'         => array(
                        'type'                  => 'select',
                        'label'                 => __('Stop On Hover', 'woopack'),
                        'default'               => 'yes',
                        'options'               => array(
                            'yes'                   => __('Yes', 'woopack'),
                            'no'                    => __('No', 'woopack'),
                        ),
                    ),
                    'transition_duration'   => array(
                        'type'                  => 'unit',
                        'label'                 => __('Slide Speed', 'woopack'),
                        'default'               => '1',
                        'units'					=> array( 'seconds' ),
                        'slider'				=> true,
                    ),
                    'lazy_load'             => array(
                        'type'                  => 'select',
                        'label'                 => __('Lazy Load', 'woopack'),
                        'default'               => 'yes',
                        'options'               => array(
                            'yes'                   => __('Yes', 'woopack'),
                            'no'                    => __('No', 'woopack'),
                        ),
                    ),
                    'carousel_loop'         => array(
                        'type'                  => 'select',
                        'label'                 => __('Loop', 'woopack'),
                        'default'               => 'true',
                        'options'               => array(
                            'true'                  => __('Yes', 'woopack'),
                            'false'                 => __('No', 'woopack'),
                        ),
                    ),
                ),
            ),
            'controls'  => array(
				'title'         => __('Slider Controls', 'woopack'),
				'collapsed' => true,
                'fields'        => array(
                    'owl_pagination'                => array(
                        'type'                          => 'select',
                        'label'                         => __('Slider Controls', 'woopack'),
                        'default'                       => 'dots',
                        'options'                       => array(
                            'no'       	                    => __('None', 'woopack'),
                            'dots'                          => __('Dots', 'woopack'),
                            'arrows'                        => __('Arrows', 'woopack'),
                        ),
                        'toggle'                        => array(
                            'dots'                          => array(
                                'fields'                        => array( 'owl_pagination_color', 'owl_active_pagination_color' )
                            ),
                            'arrows'                        => array(
                                'fields'                        => array( 'owl_arrow_color', 'owl_arrow_color', 'owl_arrow_bg_color', 'owl_arrow_bg_hover_color' )
                            ),
                        ),
                    ),
                    'owl_pagination_color'          => array(
                        'type'                          => 'color',
                        'label'                         => __('Color', 'woopack'),
                        'show_reset'                    => true,
                        'show_alpha'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.owl-theme .owl-dots .owl-dot span',
                            'property'		                => 'background-color',
                        ),
                    ),
                    'owl_active_pagination_color'   => array(
                        'type'                          => 'color',
                        'label'                         => __('Active Color', 'woopack'),
                        'show_reset'                    => true,
                        'show_alpha'                    => true,
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.owl-theme .owl-dots .owl-dot.active span',
                            'property'		                => 'background-color',
                        ),
                    ),
                    'owl_arrow_color'               => array(
                        'type'                          => 'color',
                        'label'                         => __('Color', 'woopack'),
                        'show_reset'                    => true,
                        'show_alpha'                    => true,
                        'default'                       => '000',
                        'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.owl-theme .owl-nav button svg',
                            'property'		                => 'color',
                        ),
					),
					'owl_arrow_hover_color'         => array(
                        'type'                          => 'color',
                        'label'                         => __('Hover Color', 'woopack'),
                        'show_reset'                    => true,
                        'show_alpha'                    => true,
                    ),
                    'owl_arrow_bg_color'            => array(
                        'type'                          => 'color',
                        'label'                         => __('Background Color', 'woopack'),
                        'show_reset'                    => true,
                        'show_alpha'                    => true,
					),
					'owl_arrow_bg_hover_color'     	=> array(
                        'type'                          => 'color',
                        'label'                         => __('Background Hover Color', 'woopack'),
                        'show_reset'                    => true,
                        'show_alpha'                    => true,
                        'preview'                       => array(
                            'type'                          => 'none',
                        ),
					),
					'owl_arrow_radius'				=> array(
						'type'							=> 'unit',
						'label'							=> __('Round Corners', 'woopack'),
						'default'						=> '0',
                        'units'							=> array( '%' ),
                        'slider'						=> true,
						'preview'                       => array(
                            'type'                          => 'css',
                            'selector'                      => '.owl-theme .owl-nav button svg',
							'property'		                => 'border-radius',
							'unit'							=> '%'
                        ),
					)
                ),
			),
			'additional_settings' => array(
				'title'	=> __( 'Additional Settings', 'woopack' ),
				'collapsed' => true,
				'fields' => array(
					'no_results_message'    => array(
                        'type'                  => 'text',
                        'label'                 => __('No Results Message', 'woopack'),
                        'default'               => __("Sorry, we couldn't find any products. Please try a different search.", 'woopack'),
                        'connections'           => array('string'),
                    ),
                    'show_search'           => array(
                        'type'                  => 'select',
                        'label'                 => __('Show Search', 'woopack'),
                        'default'               => '1',
                        'options'               => array(
                            '1'                     => __('Show', 'woopack'),
                            '0'                     => __('Hide', 'woopack')
                        ),
                        'help'                  => __( 'Shows the search form if no products are found.', 'woopack' )
                    ),
				),
			),
        ),
    ),
    'content'   => array(
        'title'     => __('Content', 'woopack'),
        'file'      => WOOPACK_DIR . 'includes/loop-settings.php',
    ),
    'style'     => array(
        'title'     => __('Style', 'woopack'),
        'sections'  => woopack_product_style_fields()
    ),
    'button'    => array(
        'title'     => __('Button', 'woopack'),
        'sections'  => woopack_product_button_fields()
    ),
    'typography'=> array(
        'title'     => __('Typography', 'woopack'),
        'sections'  => woopack_product_typography_fields()
    ),
));
