<?php
	$wrapperClasses = array(
		'woopack-mini-cart',
		'woopack-desktop-align-' . $settings->cart_button_alignment,
	);

	if ( isset( $settings->cart_button_alignment_medium ) && ! empty( $settings->cart_button_alignment_medium ) ) {
		$wrapperClasses[] = 'woopack-medium-align-' . $settings->cart_button_alignment_medium;
	}
	if ( isset( $settings->cart_button_alignment_responsive ) && ! empty( $settings->cart_button_alignment_responsive ) ) {
		$wrapperClasses[] = 'woopack-responsive-align-' . $settings->cart_button_alignment_responsive;
	}

	$wrapperClasses[] = 'woocommerce';
?>
<div class="<?php echo implode( ' ', $wrapperClasses ); ?>">
	<div class="woopack-cart-button">

		<?php if ( 'before' == $settings->counter_position ) { ?>
			<span class="cart-contents-count-before">
				<span class="cart-counter"><?php echo is_object( WC()->cart ) ? WC()->cart->get_cart_contents_count() : '0'; ?></span>
			</span>
		<?php } ?>

		<a class="woopack-cart-contents <?php echo 'woopack-cart-' . $settings->icon_style;?>" href="#" title="<?php _e( 'View your shopping cart', 'woopack' ); ?>" data-target="<?php echo $settings->show_cart_on; ?>">
			<span class="cart-button-wrap">
			<?php if ( 'icon' == $settings->icon_style ) { ?>

				<?php if ( 'select_icon' == $settings->icon_type ) { ?>
					<?php if ( '' != $settings->cart_icon ) { ?>
						<span class="cart-contents-icon <?php echo $settings->cart_icon; ?>"></span>
					<?php } else { ?>	
						<span class="cart-contents-icon fa fa-shopping-cart"></span>
					<?php } ?>	
				<?php } elseif ( 'select_image' == $settings->icon_type ) {?>	
					<?php if ( isset( $settings->cart_image_src ) ) { ?>
						<img src="<?php echo $settings->cart_image_src; ?>" class="cart-contents-image" />
					<?php } ?>
				<?php } ?>

			<?php } elseif ( 'icon_text' == $settings->icon_style ) { ?>
				
				<?php if ( 'select_icon' == $settings->icon_type ) { ?>
					<?php if ( '' != $settings->cart_icon ) { ?>
						<span class="cart-contents-icon <?php echo $settings->cart_icon; ?>"></span>
					<?php } else { ?>	
						<span class="cart-contents-icon fa fa-shopping-cart"></span>
					<?php } ?>	
				<?php } elseif ( 'select_image' == $settings->icon_type ) {?>	
					<?php if ( isset( $settings->cart_image_src ) ) { ?>
						<img src="<?php echo $settings->cart_image_src; ?>" class="cart-contents-image"/>
					<?php } ?>
				<?php } ?>

				<span class="cart-contents-text"><?php echo $settings->cart_text; ?></span>
			
			<?php } else { ?>
			
				<span class="cart-contents-text"><?php echo $settings->cart_text; ?></span>
			
			<?php } ?>
			</span>

			<?php if ( 'top' == $settings->counter_position ) { ?>
				<span class="cart-contents-count">
					<span class="cart-counter"><?php echo is_object( WC()->cart ) ? WC()->cart->get_cart_contents_count() : '0'; ?></span>
				</span>
			<?php } ?>

		</a>

		<?php if ( 'after' == $settings->counter_position ) { ?>
			<span class="cart-contents-count-after">
				<span class="cart-counter"><?php echo is_object( WC()->cart ) ? WC()->cart->get_cart_contents_count() : '0'; ?></span>
			</span>
		<?php } ?>

	</div>

	<div class="woopack-cart-items">
		<div class="widget_shopping_cart_content"><?php woocommerce_mini_cart();?></div>
	</div>

</div>
