<?php
// ******************* Padding *******************
// Icon Button Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'icon_button_padding',
	'selector' 		=> ".fl-node-$id .woopack-cart-contents",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'icon_button_padding_top',
		'padding-right' 	=> 'icon_button_padding_right',
		'padding-bottom' 	=> 'icon_button_padding_bottom',
		'padding-left' 		=> 'icon_button_padding_left',
	),
) );
// Cart Counter Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'cart_counter_padding',
	'selector' 		=> ".fl-node-$id .cart-contents-count-before span,
						.fl-node-$id .cart-contents-count-after span",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'cart_counter_padding_top',
		'padding-right' 	=> 'cart_counter_padding_right',
		'padding-bottom' 	=> 'cart_counter_padding_bottom',
		'padding-left' 		=> 'cart_counter_padding_left',
	),
) );
// Item Box Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'box_padding',
	'selector' 		=> ".fl-node-$id .woopack-cart-items",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'box_padding_top',
		'padding-right' 	=> 'box_padding_right',
		'padding-bottom' 	=> 'box_padding_bottom',
		'padding-left' 		=> 'box_padding_left',
	),
) );
// Cart Item Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'cart_item_padding',
	'selector' 		=> ".fl-node-$id .woopack-cart-items ul li.woocommerce-mini-cart-item",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'cart_item_padding_top',
		'padding-right' 	=> 'cart_item_padding_right',
		'padding-bottom' 	=> 'cart_item_padding_bottom',
		'padding-left' 		=> 'cart_item_padding_left',
	),
) );
// Subtotal Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'subtotal_padding',
	'selector' 		=> ".fl-node-$id .woopack-cart-items .woocommerce-mini-cart__total.total",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'subtotal_padding_top',
		'padding-right' 	=> 'subtotal_padding_right',
		'padding-bottom' 	=> 'subtotal_padding_bottom',
		'padding-left' 		=> 'subtotal_padding_left',
	),
) );
// View Cart/Checkout Button Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_padding',
	'selector' 		=> ".fl-node-$id .woopack-cart-items .woocommerce-mini-cart__buttons .button",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'button_padding_top',
		'padding-right' 	=> 'button_padding_right',
		'padding-bottom' 	=> 'button_padding_bottom',
		'padding-left' 		=> 'button_padding_left',
	),
) );
// ******************* Border *******************
// Cart Button Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'cart_button_border_group',
	'selector' 		=> ".fl-node-$id .woopack-cart-contents",
) );
// Cart Counter Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'cart_counter_border_group',
	'selector' 		=> ".fl-node-$id .woopack-cart-button .cart-counter",
) );
// Cart Counter Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'box_border_group',
	'selector' 		=> ".fl-node-$id .woopack-cart-items",
) );
// View Button Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'view_button_border_group',
	'selector' 		=> ".fl-node-$id .woopack-cart-items .woocommerce-mini-cart__buttons .button:not(.checkout)",
) );
// Checkout Button Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'checkout_button_border_group',
	'selector' 		=> ".fl-node-$id .woopack-cart-items .woocommerce-mini-cart__buttons .button.checkout",
) );
// ******************* Typography *******************
// Cart Text Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'cart_text_typography',
	'selector' 		=> ".fl-node-$id .cart-contents-text",
) );
// Cart Counter Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'cart_counter_typography',
	'selector' 		=> ".fl-node-$id .woopack-cart-contents .cart-counter",
) );
// Product Name Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'product_name_typography',
	'selector' 		=> ".fl-node-$id .woopack-cart-items ul li.woocommerce-mini-cart-item a:not(.remove_from_cart_button)",
) );
// Quantity Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'quantity_typography',
	'selector' 		=> ".fl-node-$id .woopack-cart-items ul li.woocommerce-mini-cart-item .quantity",
) );
// Subtotal Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'subtotal_typography',
	'selector' 		=> ".fl-node-$id .woopack-cart-items .woocommerce-mini-cart__total.total",
) );
// View Cart/Checkout Button Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_typography',
	'selector' 		=> ".fl-node-$id .woopack-cart-items .woocommerce-mini-cart__buttons .button",
) );
if ( empty( $settings->button_spacing_medium ) ){
	$settings->button_spacing_medium = $settings->button_spacing;
}
if ( empty( $settings->button_spacing_responsive ) ){
	$settings->button_spacing_responsive = $settings->button_spacing;
}
?>

.fl-node-<?php echo $id; ?> .woopack-cart-contents {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_button_bg_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-contents:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_button_bg_hover_color ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->cart_button_border_color_hr ); ?>
}
.fl-node-<?php echo $id; ?> .cart-contents-text {
	<?php WooPack_Helper::print_css( 'color', $settings->cart_text_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-contents:hover .cart-contents-text {
	<?php WooPack_Helper::print_css( 'color', $settings->cart_text_hover_color ); ?>
}
.fl-node-<?php echo $id; ?> .cart-contents-icon {
	<?php WooPack_Helper::print_css( 'color', $settings->cart_icon_color ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->cart_icon_font_size, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .cart-contents-image {
	<?php WooPack_Helper::print_css( 'width', $settings->cart_icon_font_size, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'height', $settings->cart_icon_font_size, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-icon_text .cart-contents-icon {
	<?php WooPack_Helper::print_css( 'padding-right', $settings->icon_button_spacing, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-contents:hover .cart-contents-icon {
	<?php WooPack_Helper::print_css( 'color', $settings->cart_icon_hover_color ); ?>
}
.fl-node-<?php echo $id; ?> .cart-counter {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_counter_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->cart_counter_color ); ?>
	transition: all 0.2s;
}
.fl-node-<?php echo $id; ?> .cart-contents-count span {
	<?php WooPack_Helper::print_css( 'width', $settings->cart_counter_width, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'height', $settings->cart_counter_width, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'line-height', $settings->cart_counter_width, 'px' ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-cart-contents:hover .cart-counter {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_counter_bg_hover_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->cart_counter_hover_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-mini-cart {
	position: relative;
}
.fl-node-<?php echo $id; ?> .woopack-cart-items {
	<?php
	if ( $settings->box_width >= 0 ) {
		WooPack_Helper::print_css( 'width', $settings->box_width, $settings->box_width_unit );
	}
	?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->box_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'margin-top', $settings->box_margin_top, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item  {
	<?php if ( '' != $settings->cart_item_border_width ) { ?>
		<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->cart_item_border_width, 'px' ); ?>
		border-bottom-style: solid;
	<?php } ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->cart_item_border_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item:nth-child(even) {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_item_even_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item:nth-child(odd) {
	<?php WooPack_Helper::print_css( 'background-color', $settings->cart_item_odd_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a {
	text-decoration: none;
}

<?php if ( 'yes' == $settings->show_image ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a img {
		<?php WooPack_Helper::print_css( 'width', $settings->image_width, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'float', $settings->image_position ); ?>
		<?php if ( 'right' == $settings->image_position ) { ?>
		margin-right: 0;
		<?php } ?>
	}
	<?php } else { ?>
	.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a img {
		display: none;
	}
<?php } ?>

.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a.remove.remove_from_cart_button {
	<?php WooPack_Helper::print_css( 'color', $settings->product_remove_color, ' !important' ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->product_remove_bg_color, ' !important' ); ?>
	<?php if ( 'custom' == $settings->product_remove_font_size && ! empty( $settings->product_remove_font_size_custom ) ) { ?>
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_remove_font_size_custom, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'width', ( $settings->product_remove_font_size_custom + 4 ), 'px' ); ?>
		<?php WooPack_Helper::print_css( 'height', ( $settings->product_remove_font_size_custom + 4 ), 'px' ); ?>
	<?php } ?>
	<?php WooPack_Helper::print_css( 'top', $settings->cart_item_padding_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'right', $settings->cart_item_padding_top, 'px', 'right' == $settings->product_remove_position ); ?>
	<?php WooPack_Helper::print_css( 'left', $settings->cart_item_padding_top, 'px', 'left' == $settings->product_remove_position ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a:not(.remove_from_cart_button) {
	<?php WooPack_Helper::print_css( 'color', $settings->product_name_color ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_margin, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a.remove.remove_from_cart_button:hover {
	<?php WooPack_Helper::print_css( 'color', $settings->product_remove_color_hover, ' !important' ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->product_remove_bg_color_hover, ' !important' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item .quantity {
	<?php WooPack_Helper::print_css( 'color', $settings->quantity_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__total.total {
	<?php WooPack_Helper::print_css( 'color', $settings->subtotal_color ); ?>
	<?php WooPack_Helper::print_css( 'text-align', $settings->subtotal_text_align ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__total.total .amount {
	<?php WooPack_Helper::print_css( 'float', $settings->subtotal_price_align ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons {
	<?php WooPack_Helper::print_css( 'text-align', $settings->button_alignment ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:not(.checkout) {
	<?php WooPack_Helper::print_css( 'background-color', $settings->view_button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->view_button_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:not(.checkout):hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->view_button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->view_button_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->view_button_border_color_hover ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button.checkout {
	<?php WooPack_Helper::print_css( 'background-color', $settings->checkout_button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->checkout_button_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button.checkout:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->checkout_button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->checkout_button_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->checkout_button_border_color_hover ); ?>
}

<?php if ( 'full_width' == $settings->button_width ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button {
		width: 100%;
	}
	.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:first-child {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_spacing, 'px' ); ?>
	}
<?php } elseif ( 'custom' == $settings->button_width ) { ?>
	<?php if ( 50 >= $settings->button_width_custom ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button {
			width: calc( <?php echo $settings->button_width_custom;?>% - <?php echo $settings->button_spacing / 2; ?>px );
		}
		.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:first-child {
			<?php WooPack_Helper::print_css( 'margin-right', $settings->button_spacing, 'px' ); ?>
		}
	<?php } else { ?>
		.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button {
			width: <?php echo $settings->button_width_custom; ?>%;
		}
		.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:first-child {
			<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_spacing, 'px' ); ?>
		}
	<?php } ?>

<?php } elseif ( 'auto' == $settings->button_width ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button {
		width: auto;
	}
	.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:first-child {
		<?php WooPack_Helper::print_css( 'margin-right', $settings->button_spacing, 'px' ); ?>
	}
<?php } ?>

<?php
// *********************
// Media Query
// *********************
?>
@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-mini-cart .woopack-cart-button {
		<?php WooPack_Helper::print_css( 'text-align', $settings->cart_button_alignment_medium ); ?>
	}
	<?php if ( 'right' == $settings->cart_button_alignment_medium ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-mini-cart .woopack-cart-items {
			right: 0;
			left: auto;
			transform: translate(0);
		}
	<?php } elseif ( 'left' == $settings->cart_button_alignment_medium ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-mini-cart .woopack-cart-items {
			left: 0;
			right: auto;
			transform: translate(0);
		}
	<?php } elseif ( 'center' == $settings->cart_button_alignment_medium ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-mini-cart .woopack-cart-items {
			left: 50%;
			right: 50%;
			transform: translate(-50%);
		}
	<?php } ?>

	.fl-node-<?php echo $id; ?> .cart-contents-icon {
		<?php WooPack_Helper::print_css( 'font-size', $settings->cart_icon_font_size_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .cart-contents-image {
		<?php WooPack_Helper::print_css( 'width', $settings->cart_icon_font_size_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'height', $settings->cart_icon_font_size_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-cart-icon_text .cart-contents-icon {
		<?php WooPack_Helper::print_css( 'padding-right', $settings->icon_button_spacing_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .cart-contents-count span {
		<?php WooPack_Helper::print_css( 'width', $settings->cart_counter_width_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'height', $settings->cart_counter_width_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->cart_counter_width_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-cart-items {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->box_margin_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'width', $settings->box_width_medium, $settings->box_width_medium_unit ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item  {
		<?php if ( '' != $settings->cart_item_border_width ) { ?>
			<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->cart_item_border_width_medium, 'px' ); ?>
			border-bottom-style: solid;
		<?php } ?>
	}
	<?php if ( 'yes' == $settings->show_image ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a img {
			<?php WooPack_Helper::print_css( 'width', $settings->image_width_medium, 'px' ); ?>
		}
		<?php } else { ?>
		.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a img {
			display : none;
		}
	<?php } ?>
	.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a.remove.remove_from_cart_button {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_remove_font_size_custom_medium, 'px', 'custom' == $settings->product_remove_font_size ); ?>
		<?php WooPack_Helper::print_css( 'top', $settings->cart_item_padding_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'right', $settings->cart_item_padding_right_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a:not(.remove_from_cart_button) {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_margin_medium, 'px' ); ?>
	}
	<?php if ( 'full_width' == $settings->button_width ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button {
			width: 100%;
		}
		.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:first-child {
			<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_spacing_medium, 'px' ); ?>
		}
	<?php } elseif ( 'custom' == $settings->button_width ) { ?>
		<?php if ( 50 >= $settings->button_width_custom_medium ) { ?>
			.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button {
				width: calc( <?php echo $settings->button_width_custom_medium;?>% - <?php echo $settings->button_spacing_medium / 2; ?>px );
			}
			.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:first-child {
				<?php WooPack_Helper::print_css( 'margin-right', $settings->button_spacing_medium, 'px' ); ?>
			}
		<?php } else { ?>
			.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button {
				width: <?php echo $settings->button_width_custom_medium; ?>%;
			}
			.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:first-child {
				<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_spacing_medium, 'px' ); ?>
			}
		<?php } ?>

	<?php } elseif ( 'auto' == $settings->button_width ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button {
			width: auto;
		}
		.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:first-child {
			<?php WooPack_Helper::print_css( 'margin-right', $settings->button_spacing_medium, 'px' ); ?>
		}
	<?php } ?>
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-mini-cart .woopack-cart-button {
		<?php WooPack_Helper::print_css( 'text-align', $settings->cart_button_alignment_responsive ); ?>
	}
	<?php if ( 'right' == $settings->cart_button_alignment_responsive ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-mini-cart .woopack-cart-items {
			right: 0;
			left: auto;
			transform: translate(0);
		}
	<?php } elseif ( 'left' == $settings->cart_button_alignment_responsive ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-mini-cart .woopack-cart-items {
			left: 0;
			right: auto;
			transform: translate(0);
		}
	<?php } elseif ( 'center' == $settings->cart_button_alignment_responsive ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-mini-cart .woopack-cart-items {
			left: 50%;
			right: 50%;
			transform: translate(-50%);
		}
	<?php } ?>
	.fl-node-<?php echo $id; ?> .cart-contents-icon {
		<?php WooPack_Helper::print_css( 'font-size', $settings->cart_icon_font_size_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .cart-contents-image {
		<?php WooPack_Helper::print_css( 'width', $settings->cart_icon_font_size_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'height', $settings->cart_icon_font_size_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-cart-icon_text .cart-contents-icon {
		<?php WooPack_Helper::print_css( 'padding-right', $settings->icon_button_spacing_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .cart-contents-count span {
		<?php WooPack_Helper::print_css( 'width', $settings->cart_counter_width_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'height', $settings->cart_counter_width_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'line-height', $settings->cart_counter_width_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-cart-items {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->box_margin_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'width', $settings->box_width_responsive, $settings->box_width_responsive_unit ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item  {
		<?php if ( '' != $settings->cart_item_border_width ) { ?>
			<?php WooPack_Helper::print_css( 'border-bottom-width', $settings->cart_item_border_width_responsive, 'px' ); ?>
			border-bottom-style: solid;
		<?php } ?>
	}
	<?php if ( 'yes' == $settings->show_image ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a img {
			<?php WooPack_Helper::print_css( 'width', $settings->image_width_responsive, 'px' ); ?>
		}
		<?php } else { ?>
		.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a img {
			display : none;
		}
	<?php } ?>
	.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a.remove.remove_from_cart_button {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_remove_font_size_custom_responsive, 'px', 'custom' == $settings->product_remove_font_size ); ?>
		<?php WooPack_Helper::print_css( 'top', $settings->cart_item_padding_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'right', $settings->cart_item_padding_right_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-cart-items ul li.woocommerce-mini-cart-item a:not(.remove_from_cart_button) {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_margin_responsive, 'px' ); ?>
	}
	<?php if ( 'full_width' == $settings->button_width ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button {
			width: 100%;
		}
		.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:first-child {
			<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_spacing_responsive, 'px' ); ?>
		}
	<?php } elseif ( 'custom' == $settings->button_width ) { ?>
		<?php if ( 50 >= $settings->button_width_custom_responsive ) { ?>
			.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button {
				width: calc( <?php echo $settings->button_width_custom_responsive;?>% - <?php echo $settings->button_spacing_responsive / 2; ?>px );
			}
			.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:first-child {
				<?php WooPack_Helper::print_css( 'margin-right', $settings->button_spacing_responsive, 'px' ); ?>
			}
		<?php } else { ?>
			.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button {
				width: <?php echo $settings->button_width_custom_responsive; ?>%;
			}
			.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:first-child {
				<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_spacing_responsive, 'px' ); ?>
			}
		<?php } ?>

	<?php } elseif ( 'auto' == $settings->button_width ) { ?>
		.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button {
			width: auto;
		}
		.fl-node-<?php echo $id; ?> .woopack-cart-items .woocommerce-mini-cart__buttons .button:first-child {
			<?php WooPack_Helper::print_css( 'margin-right', $settings->button_spacing_responsive, 'px' ); ?>
		}
	<?php } ?>
}
