<div class="pp-content-grid-more-link pp-post-link clearfix">
    <a class="pp-content-grid-more pp-more-link-button" href="<?php echo $permalink; ?>" title="<?php echo $title_attrs; ?>"<?php echo $link_target; ?>><?php echo $settings->more_link_text; ?><span class="sr-only"><?php esc_attr_e( ' about', 'bb-powerpack' ); ?> <?php echo $title_attrs; ?></span></a>
</div>
